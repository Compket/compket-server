﻿using Newtonsoft.Json;
using OsSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compket_Server.Models
{
    public class Market : Model
    {
        public string Name { get; set; }
        public int OwnerId { get; set; }
        public string AddressCountry { get; set; }
        public string AddressCity { get; set; }
        public string AddressStreet{ get; set; }
        public string Description { get; set; }
        public int PictureId { get; set; }
        public string PhoneNum { get; set; }
    }
}
