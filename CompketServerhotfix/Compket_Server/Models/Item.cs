﻿using OsSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compket_Server.Models
{
    public class Item : Model
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int PictureId { get; set; }
        public decimal Price { get; set; }
        public Base.Defenitions.Currency Currency { get; set; }
        public int MarketId { get; set; }
        public string BarcodeNum{ get; set; }
        public int CategoryId { get; set; }
    }
}
