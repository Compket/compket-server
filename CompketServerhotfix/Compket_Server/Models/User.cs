﻿using OsSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compket_Server.Models
{
    public class User : Model
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public Base.Defenitions.AccountType AccountType { get; set; }
        public int PictureId { get; set; }
        public string PhoneNum { get; set; }
    }
}
