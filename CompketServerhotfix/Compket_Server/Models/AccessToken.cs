﻿using Compket_Server.Base;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OsSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compket_Server.Models
{
    public class AccessToken : Model
    {
        public int UserId { get; set; }
        public string Token{ get; set; }
        public string PrevToken{ get; set; }

        [JsonConverter(typeof(SecondsEpochConverter))]
        public DateTime RenewalDate { get; set; }
    }
}
