﻿using OsSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Compket_Server.Systems;

namespace Compket_Server.Models
{
    public class Picture : Model
    {
        public Base.Defenitions.FileType Extension { get; set; }
        public string Name { get; set; }
    }
}
