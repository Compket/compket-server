﻿using OsSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compket_Server.Models
{
    public class Preference : Model
    {
        public int UserId { get; set; }
        public bool GotWheels { get; set; }
    }
}
