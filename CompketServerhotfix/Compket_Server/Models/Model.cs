﻿using OsSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compket_Server.Models
{
    public class Model : IOsSqlElement
    {
        public int ID { get; set; }

        public int GetID()
        {
            return ID;
        }
    }
}
