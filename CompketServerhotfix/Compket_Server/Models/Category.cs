﻿using OsSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compket_Server.Models
{
    public class Category : Model
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int PictureId { get; set; }
    }
}
