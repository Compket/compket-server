﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Compket_Server.Systems
{
    public static class FileSystem
    {
        private static bool started = false;
        
        public static void Start()
        {
            Base.Helper.PrintCompketServerMsg(Base.Defenitions.FileSystemStarted);
            InitMainDir();
            InitPicturesDir();
            started = true;
        }

        private static bool WriteToFile(string path, byte[] data)
        {
            try
            {
                File.WriteAllBytes(path, data);
                return true;
            }
            catch(Exception e)
            {
                Base.Helper.PrintCompketServerError(e.Message, e);
                return false;
            }
        }

        public static bool DeletePicture(Base.Defenitions.FileType t, string fileName)
        {
            RetrievePicture(t, fileName, out var err, deleteFile:true);
            return err == null;
        }

        /// <summary>
        /// Saves picture to directory and returns image name(with ext).
        /// </summary>
        /// <param name="t">fileType</param>
        /// <param name="binaryData">actual file data.</param>
        /// <returns>image name, null if couldn't save.</returns>
        public static string SavePicture(Base.Defenitions.FileType t, byte[] binaryData)
        {
            if(!started)
            {
                throw DirCreationErr(null);
            }

            string generatedName = Base.Helper.GetRandomPictureName();
            string fullName = generatedName;
            string fullPath;
            switch (t)
            {
                case Base.Defenitions.FileType.JpegPicture:
                    fullName += Base.Defenitions.JpegExtension;
                    fullPath = Base.Defenitions.JpegDirectoryAbsoulutePath
                        + Base.Defenitions.FileSeperator + fullName;
                    break;
                case Base.Defenitions.FileType.JpgPicture:
                    fullName += Base.Defenitions.JpgExtension;
                    fullPath = Base.Defenitions.JpgDirectoryAbsoulutePath
                        + Base.Defenitions.FileSeperator + fullName;
                    break;
                case Base.Defenitions.FileType.PngPicture:
                    fullName += Base.Defenitions.PngExtension;
                    fullPath = Base.Defenitions.PngDirectoryAbsoulutePath
                        + Base.Defenitions.FileSeperator + fullName;
                    break;
                default:
                    fullPath = null;
                    break;
            }
            if (fullPath != null && WriteToFile(fullPath, binaryData))
                return fullName;
            return null;
        }

        public static string RetrievePicturePhysicalLocation(Base.Defenitions.FileType t, string fileName,
            out string err, bool deleteFile = false)
        {
            err = null;
            if (!started)
            {
                throw DirCreationErr(null);
            }

            string path = t switch
            {
                Base.Defenitions.FileType.JpegPicture => Base.Defenitions.JpegDirectoryAbsoulutePath,
                Base.Defenitions.FileType.JpgPicture => Base.Defenitions.JpgDirectoryAbsoulutePath,
                Base.Defenitions.FileType.PngPicture => Base.Defenitions.PngDirectoryAbsoulutePath,
                _ => null,
            };
            if (path == null)
            {
                Base.Helper.PrintCompketServerError(Base.Defenitions.FileSystemWrongTypeUsed);
                return null;
            }
            else
            {
                path += Base.Defenitions.FileSeperator + fileName;
                // Load file meta data with FileInfo
                FileInfo fileInfo = new FileInfo(path);
                if (fileInfo.Exists)
                {
                    if (deleteFile)
                    {
                        try
                        {
                            File.Delete(path);
                        }
                        catch (Exception e)
                        {
                            err = e.Message;
                            Base.Helper.PrintCompketServerError(e.Message, e);
                        }
                    }
                    else
                    {
                        return fileInfo.FullName;
                    }
                }

                Base.Helper.PrintCompketServerError(Base.Defenitions.FileSystemPathDoesntExist);
                return null;
            }
        }

        /// <summary>
        /// Finds the specific image under the right directory according to given filetype
        /// </summary>
        /// <param name="t">file type</param>
        /// <param name="fileName">file name</param>
        /// <returns>the binary image found or null</returns>
        public static byte[] RetrievePicture(Base.Defenitions.FileType t, string fileName,
            out string err, bool deleteFile = false)
        {
            err = null;
            if (!started)
            {
                throw DirCreationErr(null);
            }

            string path = t switch
            {
                Base.Defenitions.FileType.JpegPicture => Base.Defenitions.JpegDirectoryAbsoulutePath,
                Base.Defenitions.FileType.JpgPicture => Base.Defenitions.JpgDirectoryAbsoulutePath,
                Base.Defenitions.FileType.PngPicture => Base.Defenitions.PngDirectoryAbsoulutePath,
                _ => null,
            };
            if (path == null)
            {
                Base.Helper.PrintCompketServerError(Base.Defenitions.FileSystemWrongTypeUsed);
                return null;
            }
            else
            {
                path += Base.Defenitions.FileSeperator + fileName;
                // Load file meta data with FileInfo
                FileInfo fileInfo = new FileInfo(path);
                if(fileInfo.Exists)
                {
                    if (deleteFile)
                    {
                        try
                        {
                            File.Delete(path);
                        }
                        catch (Exception e)
                        {
                            err = e.Message;
                            Base.Helper.PrintCompketServerError(e.Message, e);
                        }
                    }
                    else
                    {
                        // The byte[] to save the data in
                        byte[] data = new byte[fileInfo.Length];

                        // Load a filestream and put its content into the byte[]
                        using (FileStream fs = fileInfo.OpenRead())
                        {
                            fs.Read(data, 0, data.Length);
                            fs.Close();
                        }

                        return data;
                    }
                }
                Base.Helper.PrintCompketServerError(Base.Defenitions.FileSystemPathDoesntExist);
                return null;
            }
        }
        private static Exception DirCreationErr(Exception e)
        {
            var err = Base.Defenitions.FileDirectoriesWasntInitialized
                    + ' ' + e.Message;
            Base.Helper.PrintCompketServerError(err, e);
            return new Exception(err);
        }
        private static void InitMainDir()
        {
            try
            {
                System.IO.Directory.CreateDirectory(Base.Defenitions.MainDirectoryRelativePath);
            }
            catch (Exception e)
            {
                throw DirCreationErr(e);
            }
        }
        
        private static void InitPicturesDir()
        {
            try
            {
                System.IO.Directory.CreateDirectory(Base.Defenitions.PicturesDirectoryAbsoulutePath);
                System.IO.Directory.CreateDirectory(Base.Defenitions.PngDirectoryAbsoulutePath);
                System.IO.Directory.CreateDirectory(Base.Defenitions.JpegDirectoryAbsoulutePath);
                System.IO.Directory.CreateDirectory(Base.Defenitions.JpgDirectoryAbsoulutePath);
            }
            catch (Exception e)
            {
                throw DirCreationErr(e);
            }
        }
    }
}
