﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Compket_Server.Models;
using Compket_Server.Base;
using static Compket_Server.Base.ControllerInputStructs;

namespace Compket_Server.Systems
{
    public static class Items
    {
        public static Item FindItemByIdInDb(int id)
        {
            return Database.GetSingleObjectFromDB<Item>(Database.TableType.Items,
                nameof(Item.ID) + Defenitions.EqualsStatement +
                Defenitions.ParameterStatement + nameof(Item.ID),
                out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(Item.ID), id));
        }

        public static string GetItemPictureLocation(int itemId, out Defenitions.FileType imageType)
        {
            imageType = Defenitions.FileType.NotAPicture;
            if (itemId <= Defenitions.UndefinedId)
                return null;
            var item = FindItemByIdInDb(itemId);
            return item != null ? Pictures.GetPicturePhysicalLocation(item.PictureId, out imageType) : null;
        }

        /// <summary>
        /// Looks for an item in the db with given name/barcode (which isn't unique in the db)
        /// that is associated to a specific market.
        /// </summary>
        /// <param name="marketId">the specific market</param>
        /// <param name="name">the item's name</param>
        /// <param name="barcode">the item's barcode</param>
        /// <returns>null if not found</returns>
        public static Item FindItemByNameBarcodeInMarketInDb(int marketId, string name = null, 
            string barcode = null)
        {
            if (name == null && barcode == null)
                return null;
            string nameQuery = name == null ? null : 
                nameof(Item.MarketId) + Defenitions.EqualsStatement +
                Defenitions.ParameterStatement + nameof(Item.MarketId) + 
                Defenitions.AndStatement + nameof(Item.Name) + Defenitions.EqualsStatement +
                Defenitions.ParameterStatement + nameof(Item.Name);
            
            string barcodeQuery = barcode == null ? null : 
                nameof(Item.MarketId) + Defenitions.EqualsStatement +
                Defenitions.ParameterStatement + nameof(Item.MarketId) +
                Defenitions.AndStatement + nameof(Item.BarcodeNum) + Defenitions.EqualsStatement +
                Defenitions.ParameterStatement + nameof(Item.BarcodeNum);

            var parm = new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    (nameQuery == null ? nameof(Item.BarcodeNum) : nameof(Item.Name)),
                    nameQuery == null ? barcode : name);

            return Database.GetSingleObjectFromDB<Item>(Database.TableType.Items,
                nameQuery == null ? barcodeQuery : nameQuery,
                out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                nameof(Item.MarketId), marketId), parm);
                
        }

        public static List<Item> FindAllByCategoryAndNameOrBarcode(int categoryId, string name = null, string barcodeNum = null)
        {
            if (name == "")
                name = null;
            if (barcodeNum == "")
                barcodeNum = null;
            if (categoryId <= Defenitions.UndefinedId && name == null && barcodeNum == null)
                return null;
            else if (name == null && barcodeNum == null)
                return FindAllByCategoryId(categoryId, true);
            else
            {
                return FindAllByNameOrBarcode(name, barcodeNum, true, categoryId);
            }
        }

        /// <summary>
        /// Finds all items by specific name/barcode from all markets.
        /// should call with name or barcode but not both.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="barcode"></param>
        /// <returns></returns>
        private static List<Item> FindAllByNameOrBarcode(string name = null, string barcode = null, bool removeDuplicates = false, int categoryIdFilteration = Defenitions.UndefinedId)
        {
            if (name == null && barcode == null)
                return null;
            string nameQuery = name == null ? null :
                nameof(Item.Name) + Defenitions.LikeStatement +
                Defenitions.ParameterStatement + nameof(Item.Name);

            string barcodeQuery = barcode == null ? null :
                nameof(Item.BarcodeNum) + Defenitions.LikeStatement +
                Defenitions.ParameterStatement + nameof(Item.BarcodeNum);
            var paramNameQuery = nameQuery == null ? nameof(Item.BarcodeNum) : nameof(Item.Name);
            var param = new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    paramNameQuery,
                    nameQuery == null ? "%" + barcode + "%" : "%" + name + "%");
            var ret = Database.SelectAsOjects<Item>(Database.TableType.Items,
                nameQuery == null ? barcodeQuery : nameQuery, param
                );
            if(!removeDuplicates)
                return ret;
            return FilterListToGenericItems(ret, categoryIdFilteration);

        }

        /// <summary>
        /// finds all items of a market
        /// </summary>
        /// <param name="mid"></param>
        /// <returns></returns>
        public static List<Item> FindAllByMarketId(int mid)
        {
            return Database.SelectAsOjects<Item>(Database.TableType.Items,
                nameof(Item.MarketId) + Defenitions.EqualsStatement +
                Defenitions.ParameterStatement + nameof(Item.MarketId),
                new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                nameof(Item.MarketId), mid));
        }

        /// <summary>
        /// removes duplicated items from list - so that we'll have generic items - by barcodes.
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        private static List<Item> FilterListToGenericItems(List<Item> list, int categoryIdFilteration = Defenitions.UndefinedId)
        {
            var filteredLst = new List<Item>();
            foreach (var item in list)
            {
                if (categoryIdFilteration != Defenitions.UndefinedId && item.CategoryId != categoryIdFilteration)
                    continue;//we skip if there is filteration by category and current item isnt relavent 
                                //to category
                if (filteredLst.Find(x => x.BarcodeNum == item.BarcodeNum) == null)
                {
                    item.Price = Defenitions.MinimumPrice;
                    filteredLst.Add(item);
                }
            }
            return filteredLst;
        }

        /// <summary>
        /// finds all items of a category
        /// can specify if we want to removeDuplicates (usage for user general item selection)
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="removeDuplicates">set as true if want to 
        /// give a list of general items with barcode of a category (disassociate from
        /// market)</param>
        /// <returns></returns>
        public static List<Item> FindAllByCategoryId(int cid, bool removeDuplicates = false)
        {
            var list = Database.SelectAsOjects<Item>(Database.TableType.Items,
                nameof(Item.CategoryId) + Defenitions.EqualsStatement +
                Defenitions.ParameterStatement + nameof(Item.CategoryId),
                new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                nameof(Item.CategoryId), cid));
            if(removeDuplicates)
            {

                return FilterListToGenericItems(list);
                // should have filtered list with general items only for user to use in
                // selection menu
            }
            return list;
        }

        /// <summary>
        /// Generates dictionary of barcodes and corelating items of 
        /// specific market with selected general items barcode list
        /// </summary>
        /// <param name="mid">market id</param>
        /// <param name="barcodes">list of general barcodes</param>
        /// <returns>dict of barcodes as keys and values of items that relate to the market
        /// , item will be null if the barcode wasnt found in the market.</returns>
        private static Dictionary<GenericItemRequestedItem, Item> GenerateShoppingListForMarketByItemSelection(int mid, 
            List<GenericItemRequestedItem> barcodes)
        {
            var dict = new Dictionary<GenericItemRequestedItem, Item>();
            var marketItems = FindAllByMarketId(mid);
            if (marketItems == null)
                return null;
            foreach(var barcode in barcodes)
            {//going over all the given barcodes trying to find the items.
                Item foundItem = marketItems.Find(x => x.BarcodeNum == barcode.Barcode);
                dict.Add(barcode, foundItem);//would add null if didnt find.
            }
            return dict;
        }

        /// <summary>
        /// Generaets all the shopping lists from selected markets with selected 
        /// general barcodes.
        /// </summary>
        /// <param name="marketIds">the markets we want to search</param>
        /// <param name="barcodes">the barcodes of the general items.</param>
        /// <returns>Dict with [marketIds] as keys, [dictionary of barcodes and items] as values.</returns>
        public static Dictionary<Market, Dictionary<GenericItemRequestedItem, Item>> 
            GenerateListsForGivenMarketsWithGivenBarcodes(
            List<Market> markets, List<GenericItemRequestedItem> barcodes)
        {
            var dict = new Dictionary<Market, Dictionary<GenericItemRequestedItem, Item>>();
            foreach (var market in markets)
                dict.Add(market, GenerateShoppingListForMarketByItemSelection(market.ID, barcodes));
            return dict;
        }

        /// <summary>
        /// checks if the market id and category id exist
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        private static bool ProvidedIdsValid(Item i, out string err, out int errCode)
        {
            if(!Markets.MarketExists(i.MarketId))
            {
                err = Defenitions.ErrItemMarketIdDoesntExist;
                errCode = (int)Defenitions.ErrCodes.ErrItemMarketIdDoesntExist;
                return false;
            }
            else if(Categories.FindCategoryByIdInDb(i.CategoryId) == null)
            {
                err = Defenitions.ErrItemCategoryDoesntExist;
                errCode = (int)Defenitions.ErrCodes.ErrItemCategoryDoesntExist;
                return false;
            }
            err = null;
            errCode = Defenitions.UndefinedErrCode;
            return true;
        }

        /// <summary>
        /// Adds item to db, associated with specific market.
        /// </summary>
        /// <param name="marketId"></param>
        /// <param name="name"></param>
        /// <param name="barcode"></param>
        /// <param name="price"></param>
        /// <param name="currency"></param>
        /// <param name="categoryId"></param>
        /// <param name="err"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static int AddItem(int marketId, string name, string barcode,
            decimal price, Defenitions.Currency currency, int categoryId, out string err ,
            out int errCode, string description = null)
        {
            var i = new Item()
            { 
                CategoryId = categoryId,
                BarcodeNum = barcode,
                Currency = currency,
                Description = description,
                MarketId = marketId,
                Name = name,
                Price = price,
                PictureId = Defenitions.UndefinedId
            };
            err = null;
            if (!ItemDataValid(i))
            {
                err = Defenitions.ErrItemStructureInvalid;
                errCode = (int)Defenitions.ErrCodes.ErrItemStructureInvalid;
                return Defenitions.UndefinedId;
            }
            if (!ProvidedIdsValid(i, out err, out errCode))
                return Defenitions.UndefinedId;
            if (FindItemByNameBarcodeInMarketInDb(marketId, name: name) != null
                || FindItemByNameBarcodeInMarketInDb(marketId, barcode: barcode) != null)
            {// if the item exists at this market with this name/barcode
                err = Defenitions.ErrItemExistsAtThisMarket;
                errCode = (int)Defenitions.ErrCodes.ErrItemExistsAtThisMarket;
                return Defenitions.UndefinedId;
            }
            try
            {
                return Database.Insert(Database.TableType.Items, i);
            }
            catch (Exception e)
            {
                err = e.Message;
                Helper.PrintCompketServerError(e.Message, e);
            }
            return Defenitions.UndefinedId; ;
        }

        /// <summary>
        /// Edits item dynamically, should contain whichever data we wish to have at the db
        /// </summary>
        /// <param name="marketId"></param>
        /// <param name="name"></param>
        /// <param name="barcode"></param>
        /// <param name="price"></param>
        /// <param name="currency"></param>
        /// <param name="categoryId"></param>
        /// <param name="err"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static bool EditItem(int itemId, int marketId, string name, string barcode,
            decimal price, Defenitions.Currency currency, int categoryId, out string err,
            out int errCode, string description = null)
        {
            var i = new Item()
            {
                CategoryId = categoryId,
                BarcodeNum = barcode,
                Currency = currency,
                Description = description,
                MarketId = marketId,
                Name = name,
                Price = price,
                PictureId = Defenitions.UndefinedId,
                ID = itemId
            };
            if (!ItemDataValid(i))
            {
                err = Defenitions.ErrItemStructureInvalid;
                errCode = (int)Defenitions.ErrCodes.ErrItemStructureInvalid;
                return false;
            }
            if (!ProvidedIdsValid(i, out err, out errCode))
                return false;
            var found = FindItemByIdInDb(itemId);
            if(found == null)
            {
                err = Defenitions.ErrItemDoesntExist;
                errCode = (int)Defenitions.ErrCodes.ErrItemDoesntExist;
                return false;
            }
            if((name != found.Name && FindItemByNameBarcodeInMarketInDb(marketId, name: name) != null)
                || (barcode != found.BarcodeNum &&
                FindItemByNameBarcodeInMarketInDb(marketId, barcode: barcode) != null))
            {// need to check if new name or barcode already taken
                err = Defenitions.ErrItemExistsAtThisMarket;
                errCode = (int)Defenitions.ErrCodes.ErrItemExistsAtThisMarket;
                return false;
            }
            found.BarcodeNum = i.BarcodeNum;
            found.CategoryId = i.CategoryId;
            found.Currency = i.Currency;
            found.Description = i.Description;
            found.MarketId = i.MarketId;
            found.Name = i.Name;
            found.Price = i.Price;
            return UpdateItem(found, out err);
        }

        /// <summary>
        /// Should hold correct ID
        /// </summary>
        /// <param name="item"></param>
        /// <param name="err"></param>
        /// <returns></returns>
        private static bool UpdateItem(Item item, out string err)
        {
            err = null;
            try
            {
                Database.Update(Database.TableType.Items, item,
                nameof(Item.ID) +
                Defenitions.EqualsStatement + Defenitions.ParameterStatement
                + nameof(Item.ID),
                new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(Item.ID), item.ID));
                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                Helper.PrintCompketServerError(err, e);
            }
            return false;
        }

        private static bool ItemDataValid(Item i)
        {
            return !string.IsNullOrWhiteSpace(i.BarcodeNum) && !string.IsNullOrWhiteSpace(i.Name)
                && i.MarketId > Defenitions.UndefinedId && i.Price >= Defenitions.MinimumPrice
                && i.CategoryId > Defenitions.UndefinedId;
        }

        /// <summary>
        /// sets picture
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="pictureData"></param>
        /// <param name="fileType"></param>
        /// <returns>success status(true/false)</returns>
        public static bool SetItemPicture(int itemId, byte[] pictureData, Defenitions.FileType
            fileType)
        {
            var item = FindItemByIdInDb(itemId);
            DeleteItemPicture(itemId);
            if (item != null && Pictures.SavePictureToDb(fileType, pictureData, out var pictureId))
            {
                item.PictureId = pictureId;
                return UpdateItem(item, out _);
            }
            return false;
        }

        public static byte[] GetItemPicture(int itemId, out Defenitions.FileType fileType)
        {
            fileType = Defenitions.FileType.NotAPicture;
            var item = FindItemByIdInDb(itemId);
            if (item != null)
            {
                return Pictures.GetPictureDataById(item.PictureId, out fileType);
            }
            return null;
        }

        /// <summary>
        /// deletes picture
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public static bool DeleteItemPicture(int itemId)
        {
            var item = FindItemByIdInDb(itemId);
            if (item != null && item.PictureId != Defenitions.UndefinedId &&
                Pictures.DeletePicture(item.PictureId))
            {
                item.PictureId = Defenitions.UndefinedId;
                return UpdateItem(item, out _);
            }
            return false;
        }

        /// <summary>
        /// true or false if succceded
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public static bool DeleteItem(int itemId)
        {
            try
            {
                DeleteItemPicture(itemId);
                Database.Delete(Database.TableType.Items, nameof(Item.ID)
                  + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                  + nameof(Item.ID), new OsSql.OsSqlTypes.Parameter(
                      Defenitions.ParameterStatement + nameof(Item.ID), itemId));
                return true;
            }
            catch (Exception e)
            {
                Helper.PrintCompketServerError(e.Message, e);
                return false;
            }
        }
    }
}
