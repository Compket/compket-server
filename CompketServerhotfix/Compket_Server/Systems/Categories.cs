﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Compket_Server.Base;
using Compket_Server.Models;

namespace Compket_Server.Systems
{
    public static class Categories
    {
        private static Category FindCategoryByNameInDb(string name)
        {
            Category category = new Category()
            {
                Name = name
            };
            return Database.GetSingleObjectFromDB<Category>(Database.TableType.Catgories,
                nameof(category.Name) + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                + nameof(category.Name), out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(category.Name), category.Name));
        }

        /// <summary>
        /// Gets all categories into a list
        /// if safeData=true -> will return list with safe data only.
        /// </summary>
        /// <returns></returns>
        public static List<Category> ListCategories(bool safeData = false)
        {
            var catgoryStruct = new Category();
            List<Category> categories = Database.SelectAsOjects<Category>(
                Database.TableType.Catgories,
                nameof(catgoryStruct.ID) + Defenitions.GreaterThanStatement +
                Defenitions.UndefinedId);
            if (!safeData)
                return categories;
            List<Category> safeCategories = new List<Category>(categories);
            for (int i = 0; i < safeCategories.Count; i++)
                safeCategories[i] = GetSafeCatgoryData(safeCategories[i]);
            return safeCategories;
        }

        /// <summary>
        /// Strips protected data from object
        /// </summary>
        /// <param name="given"></param>
        /// <returns>stripped object</returns>
        private static Category GetSafeCatgoryData(Category given)
        {
            return given != null ? new Category() {
                Description = given.Description,
                Name = given.Name,
                PictureId = given.PictureId
            } : null;
        }
        public static Category FindCategoryByIdInDb(int id)
        {
            Category category = new Category()
            {
                ID = id
            };
            return Database.GetSingleObjectFromDB<Category>(Database.TableType.Catgories,
                nameof(category.ID) + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                + nameof(category.ID), out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(category.ID), category.ID));
        }

        /// <summary>
        /// Gets category image(if exists), can find by ID or by name since they will both be uinque!
        /// must provide a name or id!
        /// </summary>
        /// <param name="catrgoryId"></param>
        /// <param name="categoryName"></param>
        /// <returns>bytes[], may return null if image doesnt exist or category doesnt exist.</returns>
        public static byte[] GetCategoryPicture(out Defenitions.FileType fileType, int catrgoryId = Defenitions.UndefinedId,
            string categoryName = null)
        {
            fileType = Defenitions.FileType.NotAPicture;
            if (catrgoryId == Defenitions.UndefinedId && categoryName == null)
                return null;
            var category = categoryName != null ? FindCategoryByNameInDb(categoryName) :
                FindCategoryByIdInDb(catrgoryId);
            return category
                   != null ? Pictures.GetPictureDataById(
                category.PictureId, out fileType) : null;
        }

        public static string GetCategoryPictureLocation(int categoryId, out Defenitions.FileType imageType)
        {
            imageType = Defenitions.FileType.NotAPicture;
            if (categoryId <= Defenitions.UndefinedId)
                return null;
            var category = FindCategoryByIdInDb(categoryId);
            return category != null ? Pictures.GetPicturePhysicalLocation(category.PictureId, out imageType) : null;
        }
            /// <summary>
            /// addes new category, must provide name.
            /// </summary>
            /// <param name="categoryName"></param>
            /// <param name="err"></param>
            /// <param name="description"></param>
            /// <returns></returns>
            public static int AddCategory(string categoryName, out string err, out int errCode, string description = null)
        {
            err = null;
            errCode = Defenitions.UndefinedErrCode;
            if(categoryName == null ||
                string.IsNullOrEmpty(categoryName) || string.IsNullOrWhiteSpace(categoryName) ||
                FindCategoryByNameInDb(categoryName) != null) //make sure we have unique names
            {
                err = Defenitions.ErrCategoryExistsWithGivenName;
                errCode = (int)Defenitions.ErrCodes.ErrCategoryExistsWithGivenName;
                return Defenitions.UndefinedId;
            }
            var category = new Category()
            {
                Name = categoryName,
                Description = description,
                PictureId = Defenitions.UndefinedId
            };
            try
            {
                return Database.Insert(Database.TableType.Catgories, category);
            }
            catch (Exception e)
            {
                err = e.Message;
                Helper.PrintCompketServerError(e.Message, e);
            }
            return Defenitions.UndefinedId;
        }

        private static bool UpdateCategory(Category category, out string err)
        {
            err = null;
            try
            {
                Database.Update(Database.TableType.Catgories, category,
                nameof(category.ID) +
                Defenitions.EqualsStatement + Defenitions.ParameterStatement
                + nameof(category.ID),
                new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(category.ID), category.ID));
                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                Helper.PrintCompketServerError(err, e);
            }
            return false;
        }

        /// <summary>
        /// dynamic update method, must provide name, if name is different it must be unique to table.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="categoryName"></param>
        /// <param name="err"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static bool UpdateCategory(int categoryId, string categoryName, 
            out string err, out int errCode, string description = null)
        {
            err = null;
            errCode = Defenitions.UndefinedErrCode;
            var category = FindCategoryByIdInDb(categoryId);
            if(category == null)
            {
                err = Defenitions.ErrCategoryDoesntExists;
                errCode = (int)Defenitions.ErrCodes.ErrCategoryDoesntExists;
                return false;
            }
            if(categoryName != category.Name)
            {
                if(FindCategoryByNameInDb(categoryName) != null)
                {
                    err = Defenitions.ErrCategoryExistsWithGivenName;
                    errCode = (int)Defenitions.ErrCodes.ErrCategoryExistsWithGivenName;
                    return false;
                }
                category.Name = categoryName;
            }
            category.Description = description;
            return UpdateCategory(category, out err);
        }

        /// <summary>
        /// sets picture
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="pictureData"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public static bool SetCategoryPicture(int categoryId, byte[] pictureData, Defenitions.FileType
            fileType)
        {
            var category = FindCategoryByIdInDb(categoryId);
            DeleteCategoryPicture(categoryId);
            if (Pictures.SavePictureToDb(fileType, pictureData, out var pictureId))
            {
                category.PictureId = pictureId;
                return UpdateCategory(category, out _);
            }
            return false;
        }

        /// <summary>
        /// deletes picture
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public static bool DeleteCategoryPicture(int categoryId)
        {
            var category = FindCategoryByIdInDb(categoryId);
            if(category != null && category.PictureId != Defenitions.UndefinedId &&
                Pictures.DeletePicture(category.PictureId))
            {
                category.PictureId = Defenitions.UndefinedId;
                return UpdateCategory(category, out _);
            }
            return false;
        }

        /// <summary>
        /// returns true if user is an admin.
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static bool CanUserEditCategories(int uid)
        {
            return Users.IsUserAnAdmin(uid);
        }

        /// <summary>
        /// Deletes category with given ID
        /// </summary>
        /// <param name="cid"></param>
        /// <returns>always true unless there was an exception</returns>
        public static bool DeleteCategory(int cid)
        {
            try
            {
                DeleteCategoryPicture(cid);
                Category category = new Category();
                Database.Delete(Database.TableType.Catgories, nameof(category.ID)
                  + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                  + nameof(category.ID), new OsSql.OsSqlTypes.Parameter(
                      Defenitions.ParameterStatement + nameof(category.ID), cid));
                return true;
            }
            catch (Exception e)
            {
                Helper.PrintCompketServerError(e.Message, e);
                return false;
            }
        }
    }
}
