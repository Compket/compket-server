﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Compket_Server.Models;
using Compket_Server.Base;

namespace Compket_Server.Systems
{
    public static class Users
    {
        //returns null if not found
        private static User FindUserByEmailInDb(User user)
        {
            User found = Database.GetSingleObjectFromDB<User>(Database.TableType.Users,
                nameof(user.Email) + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                + nameof(user.Email), out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement + 
                    nameof(user.Email), user.Email));
            return found;
        }
        
        //returns null if not found
        private static User FindUserByIdInDb(User user)
        {
            User found = Database.GetSingleObjectFromDB<User>(Database.TableType.Users,
                nameof(user.ID) + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                + nameof(user.ID), out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement + 
                    nameof(user.ID), user.ID));
            return found;
        }
        //returns null if not found
        private static User FindUserByPhoneInDb(User user)
        {
            User found = Database.GetSingleObjectFromDB<User>(Database.TableType.Users,
                nameof(user.PhoneNum) + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                + nameof(user.PhoneNum), out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement + 
                    nameof(user.PhoneNum), user.PhoneNum));
            return found;
        }

        /// <summary>
        /// null if uid doesnt exist
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static string GetUserPhone(int uid)
        {
            var u = FindUserByIdInDb(
            new User() { ID = uid });
            return u != null ? u.PhoneNum : null;
        }

        //returns null if not found
        private static User FindUserByNameInDb(User user)
        {
            User found = Database.GetSingleObjectFromDB<User>(Database.TableType.Users,
                nameof(user.FirstName) + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                + nameof(user.FirstName) + Defenitions.AndStatement +
                nameof(user.LastName) + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                + nameof(user.LastName), out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(user.FirstName), user.FirstName), 
                new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(user.LastName), user.LastName));
            return found;
        }

        /// <summary>
        /// Checks if user valid, if not returns false with err.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="err"></param>
        /// <returns></returns>
        private static bool IsUserStructureValid(User user, out string err, out int errCode,
            bool changePass = true)
        {
            err = null;
            errCode = Defenitions.UndefinedErrCode;
            if (changePass && 
                (user.Password == null || 
                user.Password?.Length < Defenitions.MinimumPasswordSize))
            {
                err = Defenitions.ErrUserPasswordIsTooShort;
                errCode = (int)Defenitions.ErrCodes.ErrUserPasswordIsTooShort;
                return false;
            }
            else if (string.IsNullOrEmpty(user.Email) || string.IsNullOrWhiteSpace(user.Email)
                || string.IsNullOrEmpty(user.FirstName) || string.IsNullOrWhiteSpace(user.FirstName)
                || string.IsNullOrEmpty(user.LastName) || string.IsNullOrWhiteSpace(user.LastName)
                || string.IsNullOrEmpty(user.PhoneNum) || string.IsNullOrWhiteSpace(user.PhoneNum))
            {
                err = Defenitions.ErrUserStructureLacksContent;
                errCode = (int)Defenitions.ErrCodes.ErrUserStructureLacksContent;
                return false;
            }
            else if(!Helper.IsEmailValid(user.Email))
            {
                err = Defenitions.ErrUserInvalidEmail;
                errCode = (int)Defenitions.ErrCodes.ErrUserInvalidEmail;
                return false;
            }
            else if(!Helper.IsPhoneNumber(user.PhoneNum))
            {
                err = Defenitions.ErrUserInvalidPhone;
                errCode = (int)Defenitions.ErrCodes.ErrUserInvalidPhone;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Attempts to add user obj to db, checks if he exists by full name / by email.
        /// </summary>
        /// <param name="newUser">the new user object, should have email, full name, hashed password without the salt
        /// and relavent account type, phone num as well.</param>
        /// <param name="err">relevant error if already exists.</param>
        /// <returns>true if created false if not, out string err if there is an error.</returns>
        public static bool AddUser(User newUser, out string err, out int errCode)
        {
            bool emailExists = false;
            err = null;
            errCode = Defenitions.UndefinedErrCode;
            User found = FindUserByNameInDb(newUser);
            bool nameExists;
            bool phoneExists = false;
            if (nameExists = found != null)
            {
                err = Defenitions.ErrUserExistsWithGivenName;
                errCode = (int)Defenitions.ErrCodes.ErrUserExistsWithGivenName;
            }
            else
            {
                found = FindUserByEmailInDb(newUser);
                if (emailExists = found != null)
                {
                    err = Defenitions.ErrUserExistsWithGivenEmail;
                    errCode = (int)Defenitions.ErrCodes.ErrUserExistsWithGivenEmail;
                }
                else
                {
                    found = FindUserByPhoneInDb(newUser);
                    if (phoneExists = found != null)
                    {
                        err = Defenitions.ErrUserExistsWithGivenPhone;
                        errCode = (int)Defenitions.ErrCodes.ErrUserExistsWithGivenPhone;
                    }
                }
            }
            if (!nameExists && !emailExists && !phoneExists && IsUserStructureValid(newUser, out err, 
                out errCode))
            {
                // add user to db
                try
                {
                    PerformPasswordEncryption(newUser);
                    var id = Database.Insert(Database.TableType.Users, newUser);
                    Preferences.AddPreference(id, out err, out errCode);
                    return true;
                }
                catch(Exception e)
                {
                    err = e.Message;
                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// Performs password encryption - sets salt for the user and encrypts the password with it.
        /// </summary>
        /// <param name="givenUser"></param>
        /// <returns>user with the encrypted password</returns>
        private static User PerformPasswordEncryption(User givenUser)
        {
            givenUser.Salt = Helper.GetSalt();
            givenUser.Password = Helper.HashSHA_512(givenUser.Password + givenUser.Salt);
            return givenUser;
        }
        /// <summary>
        /// Checks if user exists, outputs the found user
        /// </summary>
        /// <param name="user">user data to find with</param>
        /// <param name="found">the user the was located in db</param>
        /// <returns>true if exists</returns>
        public static bool UserExists(User user, out User found) => (found = FindUserByEmailInDb(user)) != null ||
            (found = FindUserByNameInDb(user)) != null || (found = FindUserByPhoneInDb(user)) != null ||
            (found = FindUserByIdInDb(user)) != null;

        public static bool UserExists(int uid) => FindUserByIdInDb(new User() { ID = uid }) != null;


        /// <summary>
        /// Checks if the givenUser password matches db password after encyption.
        /// </summary>
        /// <param name="givenUser">user model we want to check on</param>
        /// <param name="dbUser">user model from db</param>
        /// <returns></returns>
        private static bool UserPasswordMatchesDB(User givenUser, User dbUser)
        {
            return Helper.HashSHA_512(givenUser.Password + dbUser.Salt) == dbUser.Password;
        }

        /// <summary>
        /// Signs user in
        /// </summary>
        /// <param name="user">should hold user email and password or user phone and password</param>
        /// <returns>if successful returns access token for future communication</returns>
        public static string SignIn(User user)
        {
            // if it found a user by phone, name, email we can check password since
            // phone name and email will be unique.
            if(UserExists(user, out var found) && UserPasswordMatchesDB(user, found))
            {
                AccessTokens.ProvideSignInToken(found.ID, out var token);
                return token;
            }
            return null;
        }

        /// <summary>
        /// Gets all users into a list
        /// if safeData=true -> will return list with safe data only.
        /// </summary>
        /// <returns></returns>
        public static List<User> ListUsers(bool safeData = false)
        {
            var userStruct = new User();
            List<User> users = Database.SelectAsOjects<User>(Database.TableType.Users,
                nameof(userStruct.ID) + Defenitions.GreaterThanStatement + 
                Defenitions.UndefinedId);
            if (!safeData)
                return users;
            List<User> safeUsers = new List<User>(users);
            for(int i = 0; i < safeUsers.Count; i++)
                safeUsers[i] = GetSafeUserData(safeUsers[i]);
            return safeUsers;
        }

        /// <summary>
        /// Deletes user with given ID
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>always true unless there was an exception</returns>
        public static bool DeleteUser(int userId)
        {
            var userStruct = new User();
            try
            {
                DeleteUserPicture(userId);
                Database.Delete(Database.TableType.Users, nameof(userStruct.ID)
                  + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                  + nameof(userStruct.ID), new OsSql.OsSqlTypes.Parameter(
                      Defenitions.ParameterStatement + nameof(userStruct.ID), userId));
                var pref = Preferences.FindPreferenceByUserId(userId);
                if(pref != null)
                    Preferences.DeletePreference(pref.ID);
                return true;
            }
            catch(Exception e)
            {
                Helper.PrintCompketServerError(e.Message, e);
                return false;
            }
        }

        /// <summary>
        /// strips user from protected data.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private static User GetSafeUserData(User user)
        {
            if (user == null)
                return null;
            User ret = new User()
            {
                AccountType = user.AccountType,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhoneNum = user.PhoneNum,
                ID = user.ID,
                PictureId = user.PictureId
            };
            return ret;
        }

        /// <summary>
        /// retrieves user from db and returns with safe data only
        /// (Should not have password, salt, etc.)
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static User GetSafeUserData(int userId, out string err)
        {
            err = Defenitions.CouldNotFindObject;
            User user = new User()
            {
                ID = userId
            };
            if((user = GetSafeUserData(FindUserByIdInDb(user))) != null)
            {
                err = null;
                return user;
            }
            return null;

        }

        /// <summary>
        /// updates general user data
        /// given user should hold the correct ID,
        /// should hold hashed new password if wants to change,
        /// password should be null otherwise.
        /// and what ever data he wants to be updated(or left the same without change)
        /// e.g only wants to change name the provided email and phone should be the same.
        /// </summary>
        /// <param name="givenUser">must hold the correct ID</param>
        /// <param name="err">output for relevant error.</param>
        /// <returns>true if succeeded false if didnt</returns>
        public static bool UpdateGeneralUserData(User givenUser, out string err,
            out int errCode)
        {
            bool emailExists = false;
            bool nameExists = false;
            bool phoneExists = false;
            err = Defenitions.CouldNotFindObject;
            errCode = Defenitions.UndefinedErrCode;
            User found = FindUserByIdInDb(givenUser);
            if (found == null)
                return false;
            if ((found.FirstName != givenUser.FirstName) || (found.LastName 
                != givenUser.LastName) && /*checking if the overall name combination changed*/
                (nameExists = FindUserByNameInDb(givenUser) != null))/*If it did we 
                                                                check if that name exists*/
            {
                err = Defenitions.ErrUserExistsWithGivenName;
                errCode = (int)Defenitions.ErrCodes.ErrUserExistsWithGivenName;
            }
            else
            {
                if ((found.Email != givenUser.Email) && /*checking if the email changed*/
                (emailExists = FindUserByEmailInDb(givenUser) != null))/*If it did we test if email exists*/
                {
                    err = Defenitions.ErrUserExistsWithGivenEmail;
                    errCode = (int)Defenitions.ErrCodes.ErrUserExistsWithGivenEmail;
                }
                else
                {
                    if ((found.PhoneNum != givenUser.PhoneNum) && /*checking if the phone changed*/
                        (phoneExists = FindUserByPhoneInDb(givenUser) != null))/*If it did we test if phone exists*/
                    {
                        err = Defenitions.ErrUserExistsWithGivenPhone;
                        errCode = (int)Defenitions.ErrCodes.ErrUserExistsWithGivenPhone;
                    }
                }
            }
            if (!nameExists && !emailExists && !phoneExists && 
                IsUserStructureValid(givenUser, out err, out errCode,
                givenUser.Password != null))
            {
                // update user in db
                if (givenUser.Password != null)
                {
                    PerformPasswordEncryption(givenUser);
                    found.Password = givenUser.Password;
                    found.Salt = givenUser.Salt;
                }
                found.LastName = givenUser.LastName;
                found.FirstName = givenUser.FirstName;
                found.PhoneNum = givenUser.PhoneNum;
                return UpdateUser(found, out err);
            }
            return false;
        }

        /// <summary>
        /// Gets user picture data by userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static byte[] GetUserPicture(int userId, out Defenitions.FileType fileType)
        {
            fileType = Defenitions.FileType.NotAPicture;
            var user = FindUserByIdInDb(new User() { ID = userId });
            if(user != null)
            {
                return Pictures.GetPictureDataById(user.PictureId, out fileType);
            }
            return null;
        }

        /// <summary>
        /// should hold the correct id
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private static bool UpdateUser(User user, out string err)
        {
            err = null;
            try
            {
                Database.Update(Database.TableType.Users, user,
                nameof(user.ID) +
                Defenitions.EqualsStatement + Defenitions.ParameterStatement
                + nameof(user.ID),
                new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(user.ID), user.ID));
                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                Helper.PrintCompketServerError(err, e);
                return false;
            }
        }

        public static bool SetUserPicture(int userId, byte[] pictureData, Defenitions.FileType 
            fileType)
        {
            var user = FindUserByIdInDb(new User() { ID = userId });
            DeleteUserPicture(userId);
            if (Pictures.SavePictureToDb(fileType, pictureData, out var pictureId))
            {//success need to update db
                user.PictureId = pictureId;
                return UpdateUser(user, out _);
            }
            return false;
        }

        public static bool DeleteUserPicture(int userId)
        {
            var user = FindUserByIdInDb(new User() { ID = userId });                
            if(user != null && user.PictureId != Defenitions.UndefinedId && 
                Pictures.DeletePicture(user.PictureId))
            {
                user.PictureId = Defenitions.UndefinedId;
                return UpdateUser(user, out _);
            }
            return false;
        }

        public static bool SetUserAccountType(int userId, 
            Defenitions.AccountType accountType)
        {
            var user = FindUserByIdInDb(new User() { ID = userId });
            if (user != null)
            {
                user.AccountType = accountType;
                return UpdateUser(user, out _);
            }
            return false;
        }

        public static Defenitions.AccountType GetUserAccountType(int userId)
        {
            var user = FindUserByIdInDb(new User() { ID = userId });
            return user.AccountType;
        }

        /// <summary>
        /// returns true if a user is a verified customer.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static bool CanUserEditGeneralPrices(int userId)
        {
            Defenitions.AccountType type;
            return (type = GetUserAccountType(userId)) == Defenitions.AccountType.VerifiedCustomer || 
                type == Defenitions.AccountType.SystemAdmin;
        }

        /// <summary>
        /// validates that a user is an admin adn can perform any action.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static bool IsUserAnAdmin(int userId)
        {
            return GetUserAccountType(userId) == Defenitions.AccountType.SystemAdmin;
        }
    }
}
