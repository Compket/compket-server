﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Compket_Server.Base;
using Compket_Server.Models;

namespace Compket_Server.Systems
{
    public static class Pictures
    {
        private static Picture GetPictureById(int pictureId)
        {
            var pic = new Picture();
            return Database.GetSingleObjectFromDB<Picture>(Database.TableType.Pictures,
                nameof(pic.ID) + Defenitions.EqualsStatement 
                + Defenitions.ParameterStatement
                + nameof(pic.ID), out var _, 
                new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(pic.ID), pictureId));
        }

        public static string GetPicturePhysicalLocation(int pictureId, out Defenitions.FileType fileType)
        {
            var pic = GetPictureById(pictureId);
            fileType = pic != null ? pic.Extension : Defenitions.FileType.NotAPicture;
            return pic != null ? FileSystem.RetrievePicturePhysicalLocation(pic.Extension,
                pic.Name, out var _) : null;
        }

        private static bool SavePictureToDb(Base.Defenitions.FileType fileType, string name, 
            out int id)
        {
            try
            {
                id = Database.Insert(Database.TableType.Pictures, new Picture()
                {
                    Extension = fileType,
                    Name = name
                });
                return true;
            }
            catch(Exception e)
            {
                Helper.PrintCompketServerError(e.Message, e);
                id = Defenitions.UndefinedId;
                return false;
            }
        }

        public static byte[] GetPictureDataById(int pictureId, out Defenitions.FileType imageType)
        {
            var pic = GetPictureById(pictureId);
            imageType = pic != null ? pic.Extension : Defenitions.FileType.NotAPicture;
            return pic == null ? null : FileSystem.RetrievePicture(pic.Extension, pic.Name, 
                out var _);
        }

        public static bool DeletePicture(int pictureId)
        {
            var pic = GetPictureById(pictureId);
            if (pic != null)
            {
                if(FileSystem.DeletePicture(pic.Extension, pic.Name))
                {
                    try
                    {
                        Database.Delete(Database.TableType.Pictures, nameof(pic.ID)
                          + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                          + nameof(pic.ID), new OsSql.OsSqlTypes.Parameter(
                              Defenitions.ParameterStatement + nameof(pic.ID), pictureId));
                        return true;
                    }
                    catch (Exception e)
                    {
                        Helper.PrintCompketServerError(e.Message, e);
                        return false;
                    }
                }

            }
            return false;
        }
            /// <summary>
            /// Saves picture, returns true if successful and outputs the picId in the db.
            /// </summary>
            /// <param name="data"></param>
            /// <param name="picId"></param>
            /// <returns></returns>
            public static bool SavePictureToDb(Base.Defenitions.FileType fileType, 
            byte[] data, out int picId)
        {
            string name = null;
            if ((name = FileSystem.SavePicture(fileType, data)) != null)
            {
                return SavePictureToDb(fileType, name, out picId);
            }
            picId = Defenitions.UndefinedId;
            return false;
        }
    }
}
