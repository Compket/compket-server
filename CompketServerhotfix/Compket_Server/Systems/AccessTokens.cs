﻿using Compket_Server.Base;
using Compket_Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compket_Server.Systems
{
    public static class AccessTokens
    {

        private static bool CreateNewToken(int userId, out string token)
        {
            token = null;
            if (Users.UserExists(new User() { ID = userId }, out var _))
            {
                var found = new AccessToken()
                {
                    UserId = userId,
                    Token = Helper.GetToken(),
                    PrevToken = null,
                    RenewalDate = Helper.GenerateRenewalDate()
                };
                try
                {
                    Database.Insert(Database.TableType.AccessTokens, found);
                    token = found.Token;
                    return true;
                }
                catch (Exception e)
                {
                    Helper.PrintCompketServerError(e.Message, e);
                }
            }
            return false;
        }

        private static bool UpdateTokenInDb(AccessToken token)
        {
            try
            {
                Database.Update(Database.TableType.AccessTokens, token,
                        nameof(token.UserId) + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                        + nameof(token.UserId), new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                            nameof(token.UserId), token.UserId));
                return true;
            }
            catch(Exception e)
            {
                Helper.PrintCompketServerError(e.Message, e);
                return false;
            }
        }

        /// <summary>
        /// renews token according to correct logic,
        /// checks date, if out of date renews also if prevTokenUsed=true we set the prev token to null.
        /// </summary>
        /// <param name="found"></param>
        /// <param name="token"></param>
        /// <param name="prevTokenUsed"></param>
        /// <returns></returns>
        private static bool RenewToken(AccessToken found, out string token, bool prevTokenUsed = false)
        {
            token = null;
            if (found.RenewalDate > DateTime.UtcNow)
            {// dont need to update the token yet!
                token = found.Token;
                if (prevTokenUsed)
                {
                    found.PrevToken = null;
                    return UpdateTokenInDb(found);
                }
                return true;
            }
            else
            {
                found.PrevToken = prevTokenUsed ? null : found.Token;
                found.Token = Helper.GetToken();
                found.RenewalDate = Helper.GenerateRenewalDate();
                if (UpdateTokenInDb(found))
                {
                    token = found.Token;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Creates token if it does not exist, moves token to prevToken if exists.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>true if succeeded</returns>
        public static bool ProvideSignInToken(int userId, out string token)
        {
            AccessToken found;
            token = null;
            if((found = FindTokenByUserId(userId)) == null)
            {
                return CreateNewToken(userId, out token);
            }
            return RenewToken(found, out token);
        }

        private static AccessToken FindTokenByUserId(int userId)
        {
            AccessToken found = new AccessToken() { UserId = userId };
            found = Database.GetSingleObjectFromDB<AccessToken>(Database.TableType.AccessTokens,
                nameof(found.UserId) + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                + nameof(found.UserId), out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(found.UserId), found.UserId));
            return found;
        }
        
        /// <summary>
        /// also looks for token by previous token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private static AccessToken FindTokenByToken(string token)
        {
            AccessToken found = new AccessToken() { Token = token};
            found = Database.GetSingleObjectFromDB<AccessToken>(Database.TableType.AccessTokens,
                nameof(AccessToken.Token) + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                + nameof(AccessToken.Token) + Defenitions.OrStatement + nameof(AccessToken.PrevToken)
                + Defenitions.EqualsStatement + Defenitions.ParameterStatement + nameof(AccessToken.PrevToken)
                , out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(AccessToken.Token), token),
                new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(AccessToken.PrevToken), token));
            return found;
        }

        /// <summary>
        /// validated if the token is valid, outputs newToken if needed to update the current token.
        /// </summary>
        /// <param name="providedToken"></param>
        /// <returns>true if valid</returns>
        public static bool IsTokenValid(string providedToken, out string newToken, out AccessToken found)
        {
            newToken = null;
            found = FindTokenByToken(providedToken);
            if(found != null)
            {
                if (found.Token == providedToken)
                    return true;
                else if (found.PrevToken == providedToken)
                    return RenewToken(found, out newToken, true);
            }
            return false;
        }

        public static Defenitions.AccountType GetUserAccountType(string providedToken)
        {
            if(IsTokenValid(providedToken, out _, out var token))
            {
                if (token != null)
                    return Users.GetUserAccountType(token.UserId);
            }
            return default;
        }

        /// <summary>
        /// locates user id linked to token.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static int GetUserIdByToken(string token)
        {
            var found = FindTokenByToken(token);
            if (found != null)
                return found.UserId;
            else
                return Defenitions.UndefinedId;
        }
        
        /// <summary>
        /// method checks all tokens and updated them if needed.
        /// </summary>
        public static void RunTokenUpdateForAll()
        {
            var token = new AccessToken();
            var allTokens = Database.SelectAsOjects<AccessToken>(Database.TableType.AccessTokens,
                nameof(token.ID) + Defenitions.GreaterThanStatement + Defenitions.ParameterStatement
                          + nameof(token.ID), new OsSql.OsSqlTypes.Parameter(
                              Defenitions.ParameterStatement + nameof(token.ID),
                              Defenitions.UndefinedId));
            foreach(var t in allTokens)
            {
                RenewToken(t, out _);
            }
        }
    }
}
