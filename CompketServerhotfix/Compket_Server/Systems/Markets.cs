﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Compket_Server.Base;
using Compket_Server.Models;

namespace Compket_Server.Systems
{
    public static class Markets
    {
        private static Market FindMarketByNameInDb(string name)
        {
            Market market = new Market()
            {
                Name = name
            };
            return Database.GetSingleObjectFromDB<Market>(Database.TableType.Markets,
                nameof(market.Name) + Defenitions.EqualsStatement + 
                Defenitions.ParameterStatement + nameof(market.Name), 
                out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(market.Name), market.Name));
        }
        
        private static Market FindMarketByIdInDb(int id)
        {
            Market market = new Market()
            {
                ID = id
            };
            return Database.GetSingleObjectFromDB<Market>(Database.TableType.Markets,
                nameof(market.ID) + Defenitions.EqualsStatement + 
                Defenitions.ParameterStatement + nameof(market.ID), 
                out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(market.ID), market.ID));
        }
        
        public static List<Market> FindMarketsByOwnerIdInDb(int ownerId)
        {
            Market market = new Market()
            {
                OwnerId = ownerId
            };
            return Database.SelectAsOjects<Market>(Database.TableType.Markets,
                nameof(market.OwnerId) + Defenitions.EqualsStatement +
                Defenitions.ParameterStatement + nameof(market.OwnerId), 
                new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(market.OwnerId), market.OwnerId));
        }
        
        private static Market FindMarketByPhoneInDb(string phoneNumber)
        {
            Market market = new Market()
            {
                PhoneNum = phoneNumber
            };
            return Database.GetSingleObjectFromDB<Market>(Database.TableType.Markets,
                nameof(market.PhoneNum) + Defenitions.EqualsStatement + 
                Defenitions.ParameterStatement + nameof(market.PhoneNum), 
                out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(market.PhoneNum), market.PhoneNum));
        }

        /// <summary>
        /// Gets market image(if exists), can find by ID.
        /// </summary>
        /// <param name="marketId"></param>
        /// <returns>bytes[], may return null if image doesnt exist or market doesnt exist.</returns>
        public static byte[] GetMarketPicture(int marketId, out Defenitions.FileType imageType)
        {
            imageType = Defenitions.FileType.NotAPicture;
            if (marketId <= Defenitions.UndefinedId)
                return null;
            var market = FindMarketByIdInDb(marketId);
            return market != null ? Pictures.GetPictureDataById(market.PictureId, out imageType) : null;
        }

        public static string GetMarketPictureLocation(int marketId, out Defenitions.FileType imageType)
        {
            imageType = Defenitions.FileType.NotAPicture;
            if (marketId <= Defenitions.UndefinedId)
                return null;
            var market = FindMarketByIdInDb(marketId);
            return market != null ? Pictures.GetPicturePhysicalLocation(market.PictureId, out imageType): null;
        }

        public static List<Market> ListTopMarkets()
        {
            return Database.Top<Market>(Database.TableType.Markets, Defenitions.TopFindInDbLimit);
        }

        private static List<Market> IterateAndFindResults(string name,
            string phone, string countryName, string cityName, List<Market> markets)
        {
            List<Market> filtered = new List<Market>();
            foreach (var market in markets)
            {
                if ((name != null && market.Name.Contains(name)) ||
                    (phone != null && (market.PhoneNum.Contains(phone)||
                        market.PhoneNum.Contains(name))) 
                    ||
                    (countryName != null && (market.AddressCountry == countryName
                     && (cityName == null ||
                        cityName == market.AddressCity))))
                {
                    filtered.Add(market);
                }
            }
            return filtered;
        }

        /// <summary>
        /// Gets all markets into a list
        /// can filter markets by name, phone, 
        /// country -> and by specific city in the country(optional).
        /// </summary>
        /// <returns></returns>
        public static List<Market> ListMarkets(string countryName = null, 
            string cityName = null, string name = null, string phone = null)
        {
            List<Market> markets = Database.SelectAsOjects<Market>(
                Database.TableType.Markets,
                nameof(Market.ID) + Defenitions.GreaterThanStatement +
                Defenitions.UndefinedId);
            if (name == null && countryName == null && phone == null)
                return markets;
            var filtered = IterateAndFindResults(name, phone, countryName, cityName, markets);
            return filtered;
        }

        /// <summary>
        /// Lists markets locations.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string,List<string>> ListMarketLocations()
        {
            var markets = ListMarkets();
            if (markets == null || markets.Count == 0)
                return null;
            var dict = new Dictionary<string, List<string>>();
            foreach (var market in markets)
            {//should not have duplicates with name variation of country or city if the 
                //communication will only be between server and app, this is for 
                //sake of simplification as this project is a POC
                if(!dict.ContainsKey(market.AddressCountry))
                    dict.Add(market.AddressCountry, new List<string>());
                if (!dict[market.AddressCountry].Contains(market.AddressCity))
                    dict[market.AddressCountry].Add(market.AddressCity);
            }
            return dict;
        }

        private static bool IsMarketDataValid(string name, string country, string city,string street,
            string phoneNum, int ownerId, out string err, out int errCode)
        {
            if (string.IsNullOrWhiteSpace(name) || ownerId <= Defenitions.UndefinedId ||
                  string.IsNullOrWhiteSpace(country) || string.IsNullOrWhiteSpace(city)
                  || string.IsNullOrWhiteSpace(street)
                  || string.IsNullOrWhiteSpace(phoneNum) || !Helper.IsPhoneNumber(phoneNum))
            {
                err = Defenitions.ErrMarketExistsWithGivenNameOrPhoneEmpty;
                errCode = (int)Defenitions.ErrCodes.ErrMarketExistsWithGivenNameAtAddressOrPhoneEmpty;
                return false;
            }
            err = null;
            errCode = Defenitions.UndefinedErrCode;
            return true;
        }

        public static int AddMarket(string name, int ownerId, string country, string city, string street,
            out string err, out int errCode, string phoneNum = null, string description = null)
        {
            var market = new Market()
            {
                AddressCity = city,
                AddressCountry = country,
                AddressStreet = street,
                Description = description,
                Name = name,
                OwnerId = ownerId,
                PhoneNum = phoneNum,
                PictureId = Defenitions.UndefinedId
            };
            if (phoneNum == null)
            {
                phoneNum = Users.GetUserPhone(ownerId);
            }
            if (!IsMarketDataValid(name, country, city, street, phoneNum, ownerId, out err, out errCode))
                return Defenitions.UndefinedId;
            if(!Users.UserExists(ownerId))
            {
                err = Defenitions.ErrUserIdDoesntExist;
                errCode = (int)Defenitions.ErrCodes.ErrUserIdDoesntExist;
                return Defenitions.UndefinedId;
            }
            else if(FindMarketByNameInDb(name) != null
                || FindMarketByPhoneInDb(phoneNum) != null)
            {
                err = Defenitions.ErrMarketExistsWithGivenNameOrPhoneEmpty;
                errCode = (int)Defenitions.ErrCodes.ErrMarketExistsWithGivenNameAtAddressOrPhoneEmpty;
                return Defenitions.UndefinedId;
            }
            
            try
            {
                return Database.Insert(Database.TableType.Markets, market);
            }
            catch (Exception e)
            {
                err = e.Message;
                Helper.PrintCompketServerError(e.Message, e);
            }
            return Defenitions.UndefinedId;

        }

        /// <summary>
        /// Should hold correct ID
        /// </summary>
        /// <param name="market"></param>
        /// <param name="err"></param>
        /// <returns></returns>
        private static bool UpdateMarket(Market market, out string err)
        {
            err = null;
            try
            {
                Database.Update(Database.TableType.Markets, market,
                nameof(market.ID) +
                Defenitions.EqualsStatement + Defenitions.ParameterStatement
                + nameof(market.ID),
                new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(market.ID), market.ID));
                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                Helper.PrintCompketServerError(err, e);
            }
            return false;
        }

        /// <summary>
        /// Updates market dynamically, if name changes will check if the name already exists
        /// </summary>
        /// <param name="marketId"></param>
        /// <param name="name"></param>
        /// <param name="ownerId"></param>
        /// <param name="country"></param>
        /// <param name="city"></param>
        /// <param name="phoneNum"></param>
        /// <param name="err"></param>
        /// <param name="desciption"></param>
        /// <returns></returns>
        public static bool EditMarket(int marketId, 
            string name, int ownerId, string country, string city, string street,
            out string err, out int errCode, string phoneNum = null, string desciption = null)
        {
            if (phoneNum == null)
            {
                phoneNum = Users.GetUserPhone(ownerId);
            }
            if (!IsMarketDataValid(name, country, city, street, phoneNum, ownerId, out err, out errCode))
                return false;
            if (!Users.UserExists(ownerId))
            {
                err = Defenitions.ErrUserIdDoesntExist;
                errCode = (int)Defenitions.ErrCodes.ErrUserIdDoesntExist;
                return false;
            }
            var market = FindMarketByIdInDb(marketId);
            if(market == null)
            {
                err = null;
                errCode = (int)Defenitions.ErrCodes.ErrMarketNotExistingWithThisId;
                return false;
            }
            Market found;
            if ((market.PhoneNum != phoneNum) && (found = FindMarketByPhoneInDb(phoneNum)) != null && found.ID != marketId)
            {
                err = Defenitions.ErrMarketExistsWithGivenNameOrPhoneEmpty;
                errCode = (int)Defenitions.ErrCodes.ErrMarketExistsWithGivenNameAtAddressOrPhoneEmpty;
                return false;
            }
            if (market.Name != name)
            {
                market.Name = name;
                
                if (FindMarketByNameInDb(name) != null)
                {
                    err = Defenitions.ErrMarketExistsWithGivenNameOrPhoneEmpty;
                    errCode = (int)Defenitions.ErrCodes.ErrMarketExistsWithGivenNameAtAddressOrPhoneEmpty;
                    return false;
                }
            }
            market.OwnerId = ownerId;
            market.AddressCountry = country;
            market.AddressCity = city;
            market.AddressStreet = street;
            market.PhoneNum = phoneNum;
            market.Description = desciption;
            return UpdateMarket(market, out err);
        }

        /// <summary>
        /// sets picture
        /// </summary>
        /// <param name="marketId"></param>
        /// <param name="pictureData"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public static bool SetMarketPicture(int marketId, byte[] pictureData, Defenitions.FileType
            fileType)
        {
            var market = FindMarketByIdInDb(marketId);
            DeleteMarketPicture(marketId);
            if (Pictures.SavePictureToDb(fileType, pictureData, out var pictureId))
            {
                market.PictureId = pictureId;
                return UpdateMarket(market, out _);
            }
            return false;
        }

        /// <summary>
        /// deletes picture
        /// </summary>
        /// <param name="marketId"></param>
        /// <returns></returns>
        public static bool DeleteMarketPicture(int marketId)
        {
            var market = FindMarketByIdInDb(marketId);
            if (market != null && market.PictureId != Defenitions.UndefinedId &&
                Pictures.DeletePicture(market.PictureId))
            {
                market.PictureId = Defenitions.UndefinedId;
                return UpdateMarket(market, out _);
            }
            return false;
        }

        public static bool MarketExists(int mid) => FindMarketByIdInDb(mid) != null;

        private static bool DeleteMarketItems(int mid)
        {
            if(MarketExists(mid))
            {
                var itemLst = Items.FindAllByMarketId(mid);
                if(itemLst != null && itemLst.Count > 0)
                {
                    foreach(var item in itemLst)
                    {
                        Items.DeleteItem(item.ID);//don't care if successful or not because we need to delete the market
                                                    // should not encounter issues.
                    }
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// true or false if succceded
        /// </summary>
        /// <param name="marketId"></param>
        /// <returns></returns>
        public static bool DeleteMarket(int marketId)
        {
            try
            {
                DeleteMarketItems(marketId);
                DeleteMarketPicture(marketId);
                Database.Delete(Database.TableType.Markets, nameof(Market.ID)
                  + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                  + nameof(Market.ID), new OsSql.OsSqlTypes.Parameter(
                      Defenitions.ParameterStatement + nameof(Market.ID), marketId));
                return true;
            }
            catch (Exception e)
            {
                Helper.PrintCompketServerError(e.Message, e);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uid">user id</param>
        /// <param name="mid">market id</param>
        /// <returns></returns>
        public static bool IsUserIdOwnerOfSelectedMarket(int uid, int mid)
        {
            Market market = FindMarketByIdInDb(mid);
            return market != null && market.OwnerId == uid;
        }
    }
}
