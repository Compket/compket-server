﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Compket_Server.Base;
using Compket_Server.Models;

namespace Compket_Server.Systems
{
    public static class Preferences
    {
        public static Preference FindPreferenceById(int pid)
        {
            return Database.GetSingleObjectFromDB<Preference>(Database.TableType.Preferences,
                nameof(Preference.ID) + Defenitions.EqualsStatement +
                Defenitions.ParameterStatement + nameof(Preference.ID),
                out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(Preference.ID), pid));
        }
        
        public static Preference FindPreferenceByUserId(int uid)
        {
            return Database.GetSingleObjectFromDB<Preference>(Database.TableType.Preferences,
                nameof(Preference.UserId) + Defenitions.EqualsStatement +
                Defenitions.ParameterStatement + nameof(Preference.UserId),
                out var _, new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(Preference.UserId), uid));
        }

        public static bool AddPreference(int userId, out string err, out int errCode, bool gotWheels = false)
        {
            if(!Users.UserExists(userId))
            {
                err = Defenitions.ErrUserIdDoesntExist;
                errCode = (int)Defenitions.ErrCodes.ErrUserIdDoesntExist;
                return false;
            }
            if(FindPreferenceByUserId(userId) != null)
            {
                err = Defenitions.ErrPreferenceExists;
                errCode = (int)Defenitions.ErrCodes.ErrPreferenceExists;
                return false;
            }
            err = null;
            errCode = Defenitions.UndefinedErrCode;
            try
            {
                return Database.Insert(Database.TableType.Preferences, new Preference() { 
                    GotWheels = gotWheels,
                    UserId = userId
                })
                    != Defenitions.UndefinedId;
            }
            catch (Exception e)
            {
                err = e.Message;
                Helper.PrintCompketServerError(e.Message, e);
            }
            return false;
        }

        /// <summary>
        /// edits prefernce in db
        /// </summary>
        /// <param name="pid">prefernceId</param>
        /// <param name="uid">userId</param>
        /// <param name="err"></param>
        /// <param name="gotWheels"></param>
        /// <returns></returns>
        public static bool EditPrefernce(int pid, int uid, out string err, out int errCode, bool gotWheels = false)
        {
            if (!Users.UserExists(uid))
            {
                err = Defenitions.ErrUserIdDoesntExist;
                errCode = (int)Defenitions.ErrCodes.ErrUserIdDoesntExist;
                return false;
            }
            var found = FindPreferenceById(pid);
            if(found.UserId != uid && FindPreferenceByUserId(uid) != null)
            {
                err = Defenitions.ErrPreferenceExists;
                errCode = (int)Defenitions.ErrCodes.ErrPreferenceExists;
                return false;
            }
            found.GotWheels = gotWheels;
            return UpdatePreference(found, out err, out errCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid">preference id</param>
        /// <returns></returns>
        public static bool DeletePreference(int pid)
        {
            try
            {
                Database.Delete(Database.TableType.Preferences, nameof(Preference.ID)
                  + Defenitions.EqualsStatement + Defenitions.ParameterStatement
                  + nameof(Preference.ID), new OsSql.OsSqlTypes.Parameter(
                      Defenitions.ParameterStatement + nameof(Preference.ID), pid));
                return true;
            }
            catch (Exception e)
            {
                Helper.PrintCompketServerError(e.Message, e);
                return false;
            }
        }

        /// <summary>
        /// should hold the correct ID
        /// </summary>
        /// <param name="preference"></param>
        /// <param name="err"></param>
        /// <returns></returns>
        private static bool UpdatePreference(Preference preference, out string err, out int errCode)
        {
            err = null;
            errCode = Defenitions.UndefinedErrCode;
            try
            {
                Database.Update(Database.TableType.Preferences, preference,
                nameof(Preference.ID) +
                Defenitions.EqualsStatement + Defenitions.ParameterStatement
                + nameof(Preference.ID),
                new OsSql.OsSqlTypes.Parameter(Defenitions.ParameterStatement +
                    nameof(Preference.ID), preference.ID));
                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                errCode = (int)Defenitions.ErrCodes.GeneralServerError;
                Helper.PrintCompketServerError(err, e);
            }
            return false;
        }
    }
}
