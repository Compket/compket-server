﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OsSql;
using static OsSql.OsSqlTypes;
using MySql.Data.MySqlClient;
using Compket_Server.Base;

namespace Compket_Server.Systems
{
    public static class Database
    {
        private static SQL sql;
        private static bool initialized = false;
        private static Structure Structure_Main;
        private static bool _debug = false;
        private static Table Table_Users, Table_AccessTokens, Table_Catgories,
            Table_Items, Table_Markets, Table_Pictures, Table_Preferences;
        public enum TableType
        {
            Users,
            AccessTokens,
            Catgories,
            Items,
            Markets,
            Pictures,
            Preferences
        }
        /// <summary>
        /// Must be called prior to work with DB
        /// </summary>
        public static void Start(bool debugQueries = false)
        {
            _debug = debugQueries;
            Base.Helper.PrintCompketServerMsg(Base.Defenitions.DbStarted);
            Structure_Main = new Structure();
            Build();
            Init();
            //Connect();
            Create();
            //Base.Helper.PrintCompketServerMsg("Connection status - " + sql.Connection.State);
            //Base.Helper.PrintCompketServerMsg("Connection details - " + sql.ConnectionDetails.ToString());
            initialized = true;
        }
        
        private static Table GetTableByEnum(TableType t)
        {
            Table ret = null;
            switch (t)
            {
                case TableType.Users:
                    ret = Table_Users;
                    break;
                case TableType.AccessTokens:
                    ret = Table_AccessTokens;
                    break;
                case TableType.Catgories:
                    ret = Table_Catgories;
                    break;
                case TableType.Items:
                    ret = Table_Items;
                    break;
                case TableType.Markets:
                    ret = Table_Markets;
                    break;
                case TableType.Pictures:
                    ret = Table_Pictures;
                    break;
                case TableType.Preferences:
                    ret = Table_Preferences;
                    break;
                default:
                    break;
            }
            return ret;
        }

        private static Exception NotInitialized() => new Exception(Base.Defenitions.DatabaseWasntInitialized);
        /// <summary>
        /// select from db and get results as json list ov values
        /// </summary>
        /// <param name="t">table type</param>
        /// <param name="condition">condition to find in db</param>
        /// <returns></returns>
        public static List<Dictionary<string, object>> Select(TableType t, string condition, params Parameter[] conditionparams)
        {
            return initialized ? sql.AutoSelect(GetTableByEnum(t), condition, conditionparams: conditionparams) : throw NotInitialized();
        }
        /// <summary>
        /// insert into db
        /// </summary>
        /// <param name="t">table type</param>
        /// <param name="toInsert">object you want to insert into table as is</param>
        /// <returns></returns>
        public static int Insert(TableType t, object toInsert)
        {
            toInsert = Helper.EncodeHtmlXss(toInsert);
            return initialized ? sql.AutoInsert(toInsert, GetTableByEnum(t), skipnull: true) : throw NotInitialized();
        }

        /// <summary>
        /// Method to update db,
        /// Must ensure ID is not changed!!!
        /// findCondition to find the object we want to update..
        /// </summary>
        /// <param name="t">table type</param>
        /// <param name="findCondition">condition to find the older obj in db</param>
        /// <param name="toInsert">obj we want to insert as is</param>
        public static void Update<T>(TableType t, T toInsert, string findCondition, params Parameter[] conditionparams)
        {
            if (!initialized)
                throw NotInitialized();
            else
            {
                toInsert = (T)Helper.EncodeHtmlXss(toInsert);
                var found = GetSingleObjectFromDB<T>(t, findCondition, out var _, conditionparams: conditionparams)
                    as Models.Model;
                if (found == null)
                    return;
                (toInsert as Models.Model).ID = found.ID;
                // no need to use parameters because we update with ID key which is controlled by 
                // us abd not a user input.
                sql.AutoUpdate(toInsert, GetTableByEnum(t), nameof(found.ID) + " = " + found.ID);
            }
        }
        /// <summary>
        /// counts all apperances of object with given condition
        /// </summary>
        /// <param name="t">table type</param>
        /// <param name="findCondition">condition to look in db</param>
        /// <returns></returns>
        public static int Count(TableType t, string findCondition, params Parameter[] conditionparams)
        {
            if (!initialized)
                throw NotInitialized();
            else
                return sql.Count(GetTableByEnum(t), findCondition, conditionparams: conditionparams);
        }
        
        /// <summary>
        /// returns all from the table but limits to the amount
        /// </summary>
        /// <returns></returns>
        public static List<T> Top<T>(TableType t, int amount) where T : class, new()
        {
            if (!initialized)
                throw NotInitialized();
            else
            {
                //sql.Query("SELECT * FROM " + GetTableByEnum(t).Name + " LIMIT @amount", new Parameter("@amount", amount));
                return SelectAsOjects<T>(t, " ID > -1 LIMIT @amount", conditionparams: new Parameter("@amount", amount));
                //return sql.Select(GetTableByEnum(t), " ID > -1 LIMIT @amount", conditionparams: new Parameter("@amount", amount));
            }
        }
        /// <summary>
        /// retrives single object from db as an instance
        /// </summary>
        /// <typeparam name="T">object instance type(model)</typeparam>
        /// <param name="t">table type</param>
        /// <param name="findCondition">condition to find in db</param>
        /// <param name="data">output data from search</param>
        /// <returns></returns>
        public static T GetSingleObjectFromDB<T>(TableType t, string findCondition, 
            out Dictionary<Table.Column, object> data, params Parameter[] conditionparams)
        {
            if (!initialized)
                throw NotInitialized();
            else
            {
                try
                {
                    return sql.AutoLoad<T>(GetTableByEnum(t), findCondition, out data, conditionparams: conditionparams);
                }
                catch(Exception e)
                {
                    //sql.RefreshConnection();
                    Base.Helper.PrintCompketServerError(e.Message, e);
                    data = null;
                    return default(T);
                }
            }
                
        }
        
        /// <summary>
        /// retrives group of object instances with given condition
        /// </summary>
        /// <typeparam name="T">object instance type(model)</typeparam>
        /// <param name="t">table type</param>
        /// <param name="findCondition">condition to find in db</param>
        /// <returns></returns>
        public static List<T> SelectAsOjects<T>(TableType t, string findCondition, params Parameter[] conditionparams) where T : class , new()
        {
            if (!initialized)
                throw NotInitialized();
            else
            {
                var res = Select(t, findCondition, conditionparams: conditionparams);
                List<T> ret = new List<T>();
                for (int i = 0; i < res.Count; i++)
                {
                    ret.Add(ObjectExtensions.ToObject<T>(res[i]));
                }
                return ret;
            }
        }
        /// <summary>
        /// delete data from db in table according to condition
        /// </summary>
        /// <param name="t">table type</param>
        /// <param name="findCondition">condition to find in db</param>
        public static void Delete(TableType t, string findCondition, params Parameter[] conditionparams)
        {
            if (!initialized)
                throw NotInitialized();
            else
                sql.Delete(GetTableByEnum(t), findCondition, conditionparams: conditionparams);
        }

        private static void OsSqlDebugger_OnOsSqlDebugMessage(OsSqlDebugger.OsSqlDebugType type, string msg)
        {
            Helper.PrintCompketServerMsg("SQL Debugger (" + type + "): " + msg);
        }
        private static void OsSqlDebugger_OnOsSqlQuery(OsSqlDebugger.OsSqlDebugType type, string message)
        {
            Helper.PrintCompketServerMsg("[query] " + message);
        }
        private static void Init()
        {
            sql = new SQL(Base.Defenitions.SqlServer, Base.Defenitions.DatabaseName,
                Base.Defenitions.SqlServerUsername, Base.Defenitions.SqlServerPassword, cmt: ConnectionManagementType.Single);
            OsSqlDebugger.OnOsSqlDebugMessage += OsSqlDebugger_OnOsSqlDebugMessage;
#if SHOW_QUERIES
                        OsSqlDebugger.OnOsSqlQuery += OsSqlDebugger_OnOsSqlQuery;
#endif
            if(_debug)
                OsSqlDebugger.OnOsSqlQuery += OsSqlDebugger_OnOsSqlQuery1;
            Helper.PrintCompketServerMsg("DB Initialized.");
        }

        private static void OsSqlDebugger_OnOsSqlQuery1(int connection, string query)
        {
            Helper.PrintCompketServerMsg("[query] " + query);
        }

        private static void Connect() 
        {
            Helper.PrintCompketServerMsg("Connecting...");
            sql.Connect();
            Helper.PrintCompketServerMsg("Connection status - " + sql.Connection.State);
        }
        public static void Disconnect()
        {
            if (!initialized)
                return;
            sql.Disconnect();
            initialized = false;
            Base.Helper.PrintCompketServerMsg(Base.Defenitions.DbStopped);
        }
        private static void Create()
        {
            sql.Query(Builder.GetCreationQuery(Structure_Main));
            sql.UpdateStructure(Structure_Main, updateTables: false);
        }

        private static Table AddTableWithPostfix(string tableName) =>
            Structure_Main.AddTable(tableName + Base.Defenitions.TableNamePostfix);

        private static void Build()
        {
            Table_Users = AddTableWithPostfix(typeof(Models.User).Name);
            Table_Users.AddColumnsByClassProperties(typeof(Models.User));
            var id = Table_Users.FindC("ID");
            if (id != null)
                id.AutoInc = true;

            Table_AccessTokens = AddTableWithPostfix(typeof(Models.AccessToken).Name);
            //Table_AccessTokens.AddColumnsByAttributes(typeof(Models.AccessToken));
            Table_AccessTokens.AddColumnsByClassProperties(typeof(Models.AccessToken));
            id = Table_AccessTokens.FindC("ID");
            if (id != null)
                id.AutoInc = true;

            Table_Catgories = AddTableWithPostfix(typeof(Models.Category).Name);
            Table_Catgories.AddColumnsByClassProperties(typeof(Models.Category));
            id = Table_Catgories.FindC("ID");
            if (id != null)
                id.AutoInc = true;

            Table_Items = AddTableWithPostfix(typeof(Models.Item).Name);
            Table_Items.AddColumnsByClassProperties(typeof(Models.Item));
            id = Table_Items.FindC("ID");
            if (id != null)
                id.AutoInc = true;

            Table_Markets = AddTableWithPostfix(typeof(Models.Market).Name);
            Table_Markets.AddColumnsByClassProperties(typeof(Models.Market));
            id = Table_Markets.FindC("ID");
            if (id != null)
                id.AutoInc = true;

            Table_Pictures = AddTableWithPostfix(typeof(Models.Picture).Name);
            Table_Pictures.AddColumnsByClassProperties(typeof(Models.Picture));
            id = Table_Pictures.FindC("ID");
            if (id != null)
                id.AutoInc = true;

            Table_Preferences = AddTableWithPostfix(typeof(Models.Preference).Name);
            Table_Preferences.AddColumnsByClassProperties(typeof(Models.Preference));
            id = Table_Preferences.FindC("ID");
            if (id != null)
                id.AutoInc = true;
            Helper.PrintCompketServerMsg(Builder.GetCreationQuery(Structure_Main));
        }
    }
}
