﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compket_Server.Base
{
    public static class Defenitions
    {
        public enum AccountType
        { 
            Customer,
            VerifiedCustomer,
            MarketOwner,
            SystemAdmin
        }

        //DB defenitions
        public const string DbStarted = "Database Started";
        public const string DbStopped = "Database Stopped";
        public const string DatabaseWasntInitialized = "Database Wasn't Initialized";
        public const string FileDirectoriesWasntInitialized = "File directories could not be initialized";
        public const string FileSystemWrongTypeUsed = "While Using file system a wrong file type was given.";
        public const string FileSystemPathDoesntExist = "While Using file system a wrong file path was given.";
        public const string FileSystemStarted = "Filesystem Started";
        public static readonly DateTime Epoch = new(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public const string DatabaseName = "CompketDB";
        //SQL server must have this db name and as utf8mb4_general_ci
        public const string SqlServer = "127.0.0.1";
        public const string SqlServerUsername= "root";
        public const string SqlServerPassword = "";
        public const string TableNamePostfix = "s";

        //File systems
        public static readonly char FileSeperator = System.IO.Path.DirectorySeparatorChar;
        public const string MainDirectoryRelativePath = "Compket_Server_Saved_Files";
        public static readonly string PicturesDirectoryAbsoulutePath =  MainDirectoryRelativePath
            + FileSeperator + "Pictures";
        public static readonly string PngDirectoryAbsoulutePath = PicturesDirectoryAbsoulutePath
            + FileSeperator + "PNG";
        public static readonly string JpgDirectoryAbsoulutePath = PicturesDirectoryAbsoulutePath
            + FileSeperator + "JPG";
        public static readonly string JpegDirectoryAbsoulutePath = PicturesDirectoryAbsoulutePath
            + FileSeperator + "JPEG";
        public const string PngExtension = ".png";
        public const string JpgExtension = ".jpg";
        public const string JpegExtension = ".jpeg";
        public enum FileType
        {
            PngPicture,
            JpegPicture,
            JpgPicture,
            NotAPicture
        }

        public enum Currency
        { 
            NIS,
            USD,
            EUR
        }
        public enum ErrCodes
        {
            GeneralServerError,
            ErrUserExistsWithGivenName,
            ErrUserExistsWithGivenEmail,
            ErrUserExistsWithGivenPhone,
            ErrUserInvalidEmail,
            ErrUserInvalidPhone,
            ErrUserStructureLacksContent,
            ErrUserPasswordIsTooShort,
            ErrCategoryExistsWithGivenName,
            ErrCategoryDoesntExists,
            ErrMarketExistsWithGivenNameAtAddressOrPhoneEmpty,
            ErrUserIdDoesntExist,
            ErrPreferenceExists,
            ErrItemMarketIdDoesntExist,
            ErrItemCategoryDoesntExist,
            ErrItemStructureInvalid,
            ErrItemDoesntExist,
            ErrItemExistsAtThisMarket,
            ErrWrongCredentials,
            ErrCantSaveImage,
            ErrMarketNotExistingWithThisId,
    }


        //general
        public const int UndefinedErrCode = -1;
        public const string CompketServerError = " CompketServer Error!:";
        public const string CompketServerMsg = " CompketServer:";
        public const int SaltSize = 64;
        public const int TokenSize = 32;
        public const int PictureNameSize = 16;
        public const int MinimumPasswordSize = 8;
        public const int MaximumUserStringInfoSize = 30;
        public const int DaysToAddToRenewalDate = 1;
        public const string CouldNotFindObject = "DB could not retrieve object";

        //Users system
        public const string ErrUserExistsWithGivenName = "User already exists with given name";
        public const string ErrUserExistsWithGivenEmail = "User already exists with given email";
        public const string ErrUserExistsWithGivenPhone = "User already exists with given phone";
        public const string ErrUserInvalidEmail = "User provided invalid email.";
        public const string ErrUserInvalidPhone = "User provided invalid phone.";
        public const string ErrUserStructureLacksContent = "Provided user info is lacking content";
        public static readonly string ErrUserPasswordIsTooShort = "Provided password is too short, must have at least "
            + MinimumPasswordSize.ToString() + " characters.";
        

        //Categories system
        public const string ErrCategoryExistsWithGivenName = "Category already exists with given name";
        public const string ErrCategoryDoesntExists= "Category doesnt exist";

        //Markets system
        public const string ErrMarketExistsWithGivenNameOrPhoneEmpty = "Market exists with given name, or phone empty";
        public const string ErrUserIdDoesntExist= "Provided owner id - user doesnt exist";

        //preferences system
        public const string ErrPreferenceExists = "Preference already exists with this user id!";

        //Items system
        public const string ErrItemMarketIdDoesntExist = "Provided market id doesnt exist";
        public const string ErrItemCategoryDoesntExist = "Provided category id doesnt exist";
        public const string ErrItemStructureInvalid = "Provided item data is invalid";
        public const string ErrItemDoesntExist = "Provided item id doesnt exist";
        public const string ErrItemExistsAtThisMarket = "Item with this specific name and barcode" +
            " already exists at this market";
        public const decimal MinimumPrice = 0.0m;

        //Sql queries
        public const string AndStatement = " AND ";
        public const string OrStatement = " OR ";
        public const string EqualsStatement = " = ";
        public const string LikeStatement = " LIKE ";
        public const string ParameterStatement = "@";
        public const string GreaterThanStatement = ">";
        public const string SmallerThanStatement = "<";
        public const int UndefinedId = -1;

        public const string pngType = "image/png";
        public const string jpgType = "image/jpg";
        public const string jpegType = "image/jpeg";

        public const string UserViewMarketsNameQuery = "name";
        public const string UserViewMarketsPhoneQuery = "phone";
        public const string UserViewMarketsLocationCountryQuery = "locationcountry";
        public const string UserViewMarketsLocationCityQuery = "locationcity";

        public const int TopFindInDbLimit = 100;
    }
}
