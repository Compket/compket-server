﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Compket_Server.Base
{
    public static class Helper
    {
        /// <summary>
        /// turns dynamic obj to dict
        /// </summary>
        /// <param name="dynObj"></param>
        /// <returns></returns>
        public static Dictionary<string, object> Dyn2Dict(dynamic dynObj)
        {
            var dictionary = new Dictionary<string, object>();
            foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(dynObj))
            {
                object obj = propertyDescriptor.GetValue(dynObj);
                object o = Activator.CreateInstance(obj.GetType(), obj);
                dictionary.Add(propertyDescriptor.Name, o);
            }
            return dictionary;
        }

        /// <summary>
        /// turns dynamic that has information on the data type to a dynamic object
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        public static dynamic DynamicBodyToDynamic(dynamic body)
        {
            try
            {
                return JsonConvert.DeserializeObject<dynamic>(body.ToString());
            }
            catch(Exception e)
            {
                PrintCompketServerError(null, e);
                return null;
            }
        }


        /// <summary>
        /// Validates the provided token, should be used by controllers.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="updatedToken">updated token if needed</param>
        /// <param name="accessToken">found token</param>
        /// <returns>true if token is valid</returns>
        public static bool ControllerTokenValidator(string token, out string updatedToken, 
            out Models.AccessToken accessToken, out int errCode)
        {
            if(Systems.AccessTokens.IsTokenValid(token, out updatedToken, out accessToken))
            {
                errCode = Defenitions.UndefinedErrCode;
                return true;
            }
            else
            {
                errCode = (int)Defenitions.ErrCodes.ErrWrongCredentials;
                return false;
            }
        }

        public static bool IsFileTypeImage(string fileType, out Defenitions.FileType foundFileType)
        {
            bool isPicture = true;
            switch(fileType)
            {
                case Defenitions.pngType:
                    foundFileType = Defenitions.FileType.PngPicture;
                    break;
                case Defenitions.jpgType:
                    foundFileType = Defenitions.FileType.JpgPicture;
                    break;
                case Defenitions.jpegType:
                    foundFileType = Defenitions.FileType.JpegPicture;
                    break;
                default:
                    foundFileType = Defenitions.FileType.NotAPicture;
                    isPicture = false;
                    break;
            }
            return isPicture;
        }

        public static string EnumFileTypeToImageTypeStr(Defenitions.FileType fileType)
        {
            string foundFileType;
            switch (fileType)
            {
                case Defenitions.FileType.PngPicture:
                    foundFileType = Defenitions.pngType;
                    break;
                case Defenitions.FileType.JpgPicture:
                    foundFileType = Defenitions.jpgType;
                    break;
                case Defenitions.FileType.JpegPicture:
                    foundFileType = Defenitions.jpegType;
                    break;
                default:
                    foundFileType = null;
                    break;
            }
            return foundFileType;
        }

        /// <summary>
        /// Reades the uploaded file into a stream and returns the file bytes
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static byte[] UploadedFileToBytes(Microsoft.AspNetCore.Http.IFormFile file)
        {
            using (var s = file.OpenReadStream())
            {
                using MemoryStream ms = new();
                s.CopyTo(ms);
                byte[] fileBytes = ms.ToArray();
                return fileBytes;
            }
        }
        public static Tuple<int, string> GetErrTuple(int errCode, string err)
        {
            return new Tuple<int, string>(errCode, err);
        }
        public static Tuple<int, string> GetUserErrTuple(int errCode = (int)Defenitions.ErrCodes.ErrWrongCredentials, string err = null)
        {
            return GetErrTuple(errCode, err);
        }

        public static string GetLocalIPAddress()
        {
            try
            {
                string localIP;
                using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
                {
                    socket.Connect("8.8.8.8", 65530);
                    IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                    localIP = endPoint.Address.ToString();
                    socket.Close();
                }
                return localIP;
            }
            catch { return "127.0.0.1";}
        }
        public static Tuple<int, string> GetSuccessTuple(object data, bool toLower = true)
        {
            return new Tuple<int, string>(Defenitions.UndefinedErrCode, data != null ? ObjectExtensions.ToString(data, toLower) : "");
        }

        public static void ResetConsoleColor()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }
        public static void PrintCompketServerError(string err, Exception e = null)
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\n[SERVER] " + DateTime.UtcNow.ToString() + 
                Defenitions.CompketServerError + ' ' + (e == null ? err:
                e.Message + e.StackTrace));
            ResetConsoleColor();
        }
        public static void PrintCompketServerMsg(string msg)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n[SERVER] " + DateTime.UtcNow.ToString() + 
                Defenitions.CompketServerMsg + ' ' + msg);
            ResetConsoleColor();
        }

        /// <summary>
        /// Method encodes unsafe inputs inside object to protect against XSS
        /// </summary>
        /// <remarks>this only changes string properties inside the object.</remarks>
        /// <param name="obj">the object you want to protect</param>
        /// <returns>the safe object</returns>
        public static object EncodeHtmlXss(object obj)
        {
            try
            {
                foreach (var currProp in obj.GetType().GetProperties())
                {
                    Helper.PrintCompketServerMsg(currProp.ToString());
                    if (currProp.PropertyType == typeof(string))
                    {
                        PropertyInfo prop = obj.GetType().GetProperty(currProp.Name, BindingFlags.Public | BindingFlags.Instance);
                        if (null != prop && prop.CanWrite)
                        {
                            var str = prop.GetValue(obj) as string;
                            if (str != null && !string.IsNullOrEmpty(str))
                            {
                                /*prop.SetValue(obj,
                                      Microsoft.Security.Application.Encoder.HtmlEncode(str), null);*/
                                /*prop.SetValue(obj, "\"" +
                                      Microsoft.Security.Application.Encoder.HtmlEncode(str) + "\"", null);*/
                                //var t = System.Web.HttpUtility.HtmlDecode(prop.GetValue(obj) as string);
                                //prop.SetValue(obj, t);

                                prop.SetValue(obj,
                                      Microsoft.Security.Application.Encoder.HtmlEncode(str), null);
                            }
                        }
                    }
                }
                return obj;
            }
            catch(Exception e)
            {
                PrintCompketServerError(e.Message, e);
                return null;
            }
        }

        /// <summary>
        /// to test pictures
        /// </summary>
        /// <param name="b64"></param>
        /// <returns></returns>
        public static byte[] Base64ToBytes(string b64)
        {
            return Convert.FromBase64String(b64);
        }
        public static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return origin.AddSeconds(timestamp);
        }

        public static double ConvertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds);
        }

        public static string HashSHA_512(string value)
        {
            var data = Encoding.UTF8.GetBytes(value);
            using (SHA512 shaM = new SHA512Managed())
            {
                var bytes = shaM.ComputeHash(data);
                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        private static string GenerateRandomString(int size)
        {
            var salt = new byte[size];
            using (var random = new RNGCryptoServiceProvider())
            {
                random.GetNonZeroBytes(salt);
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < salt.Length; i++)
                {
                    builder.Append(salt[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public static string GetSalt() => GenerateRandomString(Defenitions.SaltSize);

        public static string GetToken() => GenerateRandomString(Defenitions.TokenSize);
        public static string GetRandomPictureName() => GenerateRandomString(Defenitions.PictureNameSize);
        public static DateTime GenerateRenewalDate() => DateTime.UtcNow.AddDays(Defenitions.DaysToAddToRenewalDate);

        public static bool IsEmailValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool IsPhoneNumber(string number)
        {
            return Regex.Match(number, @"^[+][0-9]{10,13}$").Success;
        }
}

    public class SecondsEpochConverter : DateTimeConverterBase
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var timeSinceEpoch = Helper.ConvertToUnixTimestamp((DateTime)value) -
                Helper.ConvertToUnixTimestamp(Defenitions.Epoch);
            writer.WriteRawValue(timeSinceEpoch.ToString());
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null) { return null; }
            return Defenitions.Epoch.AddSeconds((long)reader.Value);
        }
    }

    public class LowercaseContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.ToLower();
        }
    }

    public static class ObjectExtensions
    {
        public static T ToObject<T>(this IDictionary<string, object> source)
            where T : class, new()
        {
            var json = JsonConvert.SerializeObject(source);
            try
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch(Exception e)
            {
                Helper.PrintCompketServerError(null, e);
                return null;
            }
        }

        private static JsonSerializerSettings _settings = new JsonSerializerSettings() 
        { ContractResolver = new LowercaseContractResolver()};
        private static JsonSerializerSettings _settingsNone = new JsonSerializerSettings() 
        { ContractResolver = new DefaultContractResolver()};

        public static string ToString(object data, bool toLower = true) => data.GetType() == typeof(string) ? (string)data :
            JsonConvert.SerializeObject(data, Formatting.None, toLower ? _settings : _settingsNone);

        public static IDictionary<string, object> AsDictionary(this object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            return source.GetType().GetProperties(bindingAttr).ToDictionary
            (
                propInfo => propInfo.Name,
                propInfo => propInfo.GetValue(source, null)
            );

        }
    }
}
