﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compket_Server.Base
{
    public static class ControllerInputStructs
    {
        public struct GenericItemFiltering
        {
            public int CategoryId { get; set; }
            public string Name { get; set; }
            public string Barcode { get; set; }
        }
        public struct GenericItemRequestedItem
        {
            public string Barcode { get; set; }
            public int Amount { get; set; }

        }

        public struct GenericItemComparisonFiltering
        {
            public List<GenericItemRequestedItem> BarcodesLst {get;set;}
            public string Country { get; set; }
            public string City { get; set; }

        }

        public struct GenericItemComparisonSpecificListResult
        {
            public GenericItemRequestedItem RequestedItem { get; set; }
            public Models.Item FoundItem { get; set; }
        }

        public struct GenericItemComparisonResult
        {
            public Models.Market Market { get; set; }
            public List<GenericItemComparisonSpecificListResult> FoundItems { get; set; }
        }
    }
}
