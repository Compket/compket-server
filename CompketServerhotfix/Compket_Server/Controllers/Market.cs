﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Compket_Server.Systems;
using Compket_Server.Base;
using System.IO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Compket_Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class Market : ControllerBase
    {

        [HttpPost]
        [Route("CreateMarket")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, string>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, 
            Type = typeof(Tuple<int, string>))]
        public IActionResult CreateMarket([FromHeader] string accessToken, [FromBody] Models.Market market)
        {
            if(Helper.ControllerTokenValidator(accessToken, out var _, 
                out var foundToken, out int errCode))
            {
                int marketId;
                if ( ( marketId = Markets.AddMarket(market.Name, foundToken.UserId, market.AddressCountry,
                    market.AddressCity,market.AddressStreet, out var err, out errCode, phoneNum: market.PhoneNum,
                    description: market.Description)) != Defenitions.UndefinedId)
                {//added successfuly
                    return Ok(Helper.GetSuccessTuple(marketId));
                }
                else
                    return BadRequest(Helper.GetErrTuple(errCode, err));
            }
            else
                return BadRequest(Helper.GetErrTuple(errCode, null));
        }

        [HttpPost]
        [Route("SetMarketImage")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, string>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, 
            Type = typeof(Tuple<int, string>))]
        public IActionResult SetMarketImage([FromHeader] string accessToken, int marketId, [FromQuery] IFormFile file)
        {
            //https://stackoverflow.com/questions/67061125/upload-file-into-asp-net-core-web-api?rq=1
            if(Helper.ControllerTokenValidator(accessToken, out var _, 
                out var foundToken, out int errCode))
            {
                if(Markets.IsUserIdOwnerOfSelectedMarket(foundToken.UserId, marketId))
                {
                    if(Helper.IsFileTypeImage(file.ContentType, out var foundType))
                    {
                        var bytes = Helper.UploadedFileToBytes(file);
                        if(Markets.SetMarketPicture(marketId, bytes, foundType))
                            return Ok(Helper.GetSuccessTuple(null));
                        else//error saving image
                            return BadRequest(Helper.GetErrTuple(
                                (int)Defenitions.ErrCodes.ErrCantSaveImage, null));
                    }
                    else//not an image, bad request.
                        return BadRequest(Helper.GetErrTuple(
                            (int)Defenitions.ErrCodes.ErrCantSaveImage, null));
                }
                else
                    return BadRequest(Helper.GetErrTuple(
                            (int)Defenitions.ErrCodes.ErrWrongCredentials, null));
            }
            else
                return BadRequest(Helper.GetErrTuple(errCode, null));
        }
        
        
        [HttpGet]
        [Route("ViewOwnedMarkets")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, List<Models.Market>>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, 
            Type = typeof(Tuple<int, string>))]
        public IActionResult ViewOwnedMarkets([FromHeader] string accessToken)
        {
            if(Helper.ControllerTokenValidator(accessToken, out var _, 
                out var foundToken, out int errCode))
            {
                return Ok(Helper.GetSuccessTuple(Markets.FindMarketsByOwnerIdInDb(foundToken.UserId)));
            }
            else
                return BadRequest(Helper.GetErrTuple(errCode, null));
        }

        [HttpGet]
        [Route("GetMarketImage")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PhysicalFileResult))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult GetMarketImage([FromHeader] string accessToken, int marketId)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode))
            {
                /*var bytes = Markets.GetMarketPicture(marketId, out var type);
                if(bytes != null)
                {
                    return Ok(File(bytes, Helper.EnumFileTypeToImageTypeStr(type)));
                    
                }*/
                var location = Markets.GetMarketPictureLocation(marketId, out var type);
                if(location != null)
                {
                    return PhysicalFile(location, Helper.EnumFileTypeToImageTypeStr(type));
                }
                return NotFound();
            }
            else
                return BadRequest(Helper.GetErrTuple(errCode, null));
        }
        
        [HttpGet]
        [Route("GetCategoris")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, List<Models.Category>>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult GetCategoris([FromHeader] string accessToken)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode))
            {
                var lst = Categories.ListCategories();
                if (lst != null && lst.Count > 0)
                    return Ok(Helper.GetSuccessTuple(lst));
                return NotFound();
            }
            else
                return BadRequest(Helper.GetErrTuple(errCode, null));
        }

        [HttpPost]
        [Route("CreateCategory")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, string>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult CreateCategory([FromHeader] string accessToken, [FromBody] Models.Category category)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode) && Users.IsUserAnAdmin(foundToken.UserId))
            {// only admins are allowed to add categories
                int categoryId;
                if ((categoryId = Categories.AddCategory(category.Name, out var err, out errCode,
                    description: category.Description)) != Defenitions.UndefinedId)
                {//added successfuly
                    return Ok(Helper.GetSuccessTuple(categoryId));
                }
                else
                    return BadRequest(Helper.GetErrTuple(errCode, err));
            }
            else
                return BadRequest(Helper.GetErrTuple((int)Defenitions.ErrCodes.ErrWrongCredentials, null));
        }

        [HttpPost]
        [Route("SetCategoryImage")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, string>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult SetCategoryImage([FromHeader] string accessToken, int categoryId, [FromQuery] IFormFile file)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode) && Users.IsUserAnAdmin(foundToken.UserId))
            {// only admins can set image for category
                if (Helper.IsFileTypeImage(file.ContentType, out var foundType))
                {
                    var bytes = Helper.UploadedFileToBytes(file);
                    if ( Categories.SetCategoryPicture(categoryId, bytes, foundType))
                        return Ok(Helper.GetSuccessTuple(null));
                    else//error saving image
                        return BadRequest(Helper.GetErrTuple(
                            (int)Defenitions.ErrCodes.ErrCantSaveImage, null));
                }
                else//not an image, bad request.
                    return BadRequest(Helper.GetErrTuple(
                        (int)Defenitions.ErrCodes.ErrCantSaveImage, null));
            }
            else
                return BadRequest(Helper.GetErrTuple((int)Defenitions.ErrCodes.ErrWrongCredentials, null));
        }

        [HttpGet]
        [Route("GetCategoryImage")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PhysicalFileResult))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult GetCategoryImage([FromHeader] string accessToken, int categoryId)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode))
            {
                var location = Categories.GetCategoryPictureLocation(categoryId, out var type);
                if (location != null)
                    return PhysicalFile(location, Helper.EnumFileTypeToImageTypeStr(type));
                return NotFound();
            }
            else
                return BadRequest(Helper.GetErrTuple(errCode, null));
        }

        private static bool UserIsMarketOwner(int uid, int mid, out int errCode)
        {
            errCode = Defenitions.UndefinedErrCode;
            if(Markets.IsUserIdOwnerOfSelectedMarket(uid, mid))
                return true;
            errCode = (int)Defenitions.ErrCodes.ErrWrongCredentials;
            return false;
        }

        [HttpPost]
        [Route("createItem")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, string>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult CreateItem([FromHeader] string accessToken, int marketId,[FromBody] Models.Item item)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode) && UserIsMarketOwner(foundToken.UserId,
                    marketId, out errCode))
            {
                int itemId;
                if ((itemId = Items.AddItem(marketId, item.Name, item.BarcodeNum, item.Price, item.Currency, item.CategoryId,
                    out var err, out errCode, description: item.Description)) != Defenitions.UndefinedId)
                {//added successfuly
                    return Ok(Helper.GetSuccessTuple(itemId));
                }
                else
                    return BadRequest(Helper.GetErrTuple(errCode, err));
            }
            else
                return BadRequest(Helper.GetErrTuple(errCode, null));
        }

        [HttpPost]
        [Route("SetItemImage")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, string>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult SetItemImage([FromHeader] string accessToken, int itemId, [FromQuery] IFormFile file)
        {
            var item = Items.FindItemByIdInDb(itemId);
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode) && item != null && UserIsMarketOwner(foundToken.UserId,
                item.MarketId, out errCode))
            {// only owner can set image for item
                if (Helper.IsFileTypeImage(file.ContentType, out var foundType))
                {
                    var bytes = Helper.UploadedFileToBytes(file);
                    if (Items.SetItemPicture(itemId, bytes, foundType))
                        return Ok(Helper.GetSuccessTuple(null));
                    else//error saving image
                        return BadRequest(Helper.GetErrTuple(
                            (int)Defenitions.ErrCodes.ErrCantSaveImage, null));
                }
                else//not an image, bad request.
                    return BadRequest(Helper.GetErrTuple(
                        (int)Defenitions.ErrCodes.ErrCantSaveImage, null));
            }
            else
                return BadRequest(Helper.GetErrTuple((int)Defenitions.ErrCodes.ErrWrongCredentials, null));
        }

        [HttpGet]
        [Route("GetItemImage")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PhysicalFileResult))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult GetItemImage([FromHeader] string accessToken, int itemId)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode))
            {
                var location = Items.GetItemPictureLocation(itemId, out var type);
                if (location != null)
                    return PhysicalFile(location, Helper.EnumFileTypeToImageTypeStr(type));
                return NotFound();
            }
            else
                return BadRequest(Helper.GetErrTuple(errCode, null));
        }

        [HttpGet]
        [Route("GetMarketItems")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, List<Models.Category>>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult GetMarketItems([FromHeader] string accessToken, int marketId)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode))
            {
                var lst = Items.FindAllByMarketId(marketId);
                if (lst != null && lst.Count > 0)
                    return Ok(Helper.GetSuccessTuple(lst));
                return NotFound();
            }
            else
                return BadRequest(Helper.GetErrTuple(errCode, null));
        }

        [HttpDelete]
        [Route("deleteItem")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, string>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult DeleteItem([FromHeader] string accessToken, int marketId, int itemId)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode) && UserIsMarketOwner(foundToken.UserId,
                    marketId, out errCode))
            {
                if(Items.DeleteItem(itemId))
                {
                    return Ok(Helper.GetSuccessTuple(null));
                }
                return BadRequest(Helper.GetErrTuple(errCode, null));    
            }
            else
                return BadRequest(Helper.GetErrTuple(errCode, null));
        }

        [HttpPost]
        [Route("editItem")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, string>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult EditItem([FromHeader] string accessToken, int marketId, [FromBody] Models.Item item)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode) && UserIsMarketOwner(foundToken.UserId,
                    marketId, out errCode))
            {
                if(Items.EditItem(item.ID, marketId, item.Name, item.BarcodeNum, item.Price,
                    item.Currency, item.CategoryId, out var err, out errCode, description: item.Description))
                {
                    return Ok(Helper.GetSuccessTuple(null));
                }
                else
                    return BadRequest(Helper.GetErrTuple(errCode, err));
            }
            else
                return BadRequest(Helper.GetErrTuple(errCode, null));
        }

        [HttpDelete]
        [Route("deleteMarket")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, string>))]
        [ProducesResponseType(StatusCodes.Status404NotFound,
            Type = typeof(Tuple<int, string>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult DeleteMarket([FromHeader] string accessToken, int marketId)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode) && UserIsMarketOwner(foundToken.UserId,
                    marketId, out errCode))
            {
                if (Markets.DeleteMarket(marketId))
                {
                    return Ok(Helper.GetSuccessTuple(null));
                }
                return NotFound(Helper.GetErrTuple(errCode, null));
            }
            else
                return BadRequest(Helper.GetErrTuple(errCode, null));
        }
        
        
        [HttpPost]
        [Route("editMarket")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, string>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult EditMarket([FromHeader] string accessToken, Models.Market market)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode) && UserIsMarketOwner(foundToken.UserId,
                    market.ID, out errCode))
            {
                if(Markets.EditMarket(market.ID, market.Name, foundToken.UserId, market.AddressCountry, market.AddressCity, market.AddressStreet, out var err, out errCode, market.PhoneNum, market.Description))
                {
                    return Ok(Helper.GetSuccessTuple(null));
                }
                return BadRequest(Helper.GetErrTuple(errCode, err));
            }
            else
                return BadRequest(Helper.GetErrTuple(errCode, null));
        }
    }
}
