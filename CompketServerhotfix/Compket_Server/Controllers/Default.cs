﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Compket_Server.Systems;
using Compket_Server.Base;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Compket_Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class Default : ControllerBase
    {
       
        // GET: api/<Default>
        [HttpGet]
        [Route("Test")]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        /*
        // GET api/<Default>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<Default>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<Default>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<Default>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/

        [HttpPost]
        [Route("Register")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, string>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Tuple<int,string>))]
        public IActionResult Register([FromBody] Models.User user)
        {
            if(!Users.AddUser(new Models.User()
                {
                    AccountType = Defenitions.AccountType.Customer,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Password = user.Password,
                    PhoneNum = user.PhoneNum
                }, out var err, out int errCode))
                return BadRequest(Helper.GetErrTuple(errCode, err));
            return Ok(Helper.GetSuccessTuple(null));
        }

        /// <summary>
        /// Returns list of the access token and type of the account its associated to
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private static List<string> MakeListOfTokenAndType(string token)
        {
            var lst = new List<string>
            {
                token
            };
            var userType = AccessTokens.GetUserAccountType(token);
            lst.Add(((int)userType).ToString());
            lst.Add(AccessTokens.GetUserIdByToken(token).ToString());
            return lst;
        }
        
        [HttpPost]
        [Route("Login")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, string>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Tuple<int,string>))]
        public IActionResult Login([FromBody] Models.User user)
        {
            string token;
            if ((token = Users.SignIn(new Models.User()
                {
                    Email = user.Email,
                    Password = user.Password
                })) == null)
                return BadRequest(Helper.GetErrTuple((int)Defenitions.ErrCodes.ErrWrongCredentials,
                    null));
            return Ok(Helper.GetSuccessTuple(MakeListOfTokenAndType(token)));
        }

        [HttpGet]
        [Route("LoginToken")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, string>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Tuple<int, string>))]
        public IActionResult LoginWithToken([FromHeader] string accessToken)
        {
            if(AccessTokens.IsTokenValid(accessToken, out var newToken, out var _))
            {
                if (newToken != null)
                    accessToken = newToken;
                return Ok(Helper.GetSuccessTuple(MakeListOfTokenAndType(accessToken)));
            }
            return BadRequest(Helper.GetErrTuple((int)Defenitions.ErrCodes.ErrWrongCredentials,
                    null));
        }
    }
}
