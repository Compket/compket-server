﻿using Compket_Server.Base;
using Compket_Server.Systems;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Compket_Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class Admin : ControllerBase
    {
        private static bool CheckForAdmin(int uid, out int errCode)
        {
            if(Users.IsUserAnAdmin(uid))
            {
                errCode = Defenitions.UndefinedErrCode;
                return true;
            }
            errCode = (int)Defenitions.ErrCodes.ErrWrongCredentials;
            return false;
        }

        [HttpGet]
        [Route("GetUsers")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, List<Models.User>>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult GetUsers([FromHeader] string accessToken)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode) &&
                    CheckForAdmin(foundToken.UserId, out errCode))
            {
                    return Ok(Helper.GetSuccessTuple(Users.ListUsers(true)));
            }
            else
                return BadRequest(Helper.GetUserErrTuple(errCode));
        }

        [HttpPut]
        [Route("SetUserAccountType")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, List<Models.User>>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult SetUserAccountType([FromHeader] string accessToken, int userId, Defenitions.AccountType newType)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode) &&
                    CheckForAdmin(foundToken.UserId, out errCode))
            {
                if(Users.SetUserAccountType(userId, newType))
                    return Ok(Helper.GetSuccessTuple(null));
                return BadRequest(Helper.GetUserErrTuple(errCode));
            }
            else
                return BadRequest(Helper.GetUserErrTuple(errCode));
        }
        
        [HttpDelete]
        [Route("DeleteUser")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, List<Models.User>>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult DeleteUser([FromHeader] string accessToken, int userId)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode) &&
                    CheckForAdmin(foundToken.UserId, out errCode))
            {
                if (!CheckForAdmin(userId, out _)) // we should nt allow deletion of admins
                {
                    if (Users.DeleteUser(userId))
                        return Ok(Helper.GetSuccessTuple(null));
                }
                return BadRequest(Helper.GetUserErrTuple(errCode));
            }
            else
                return BadRequest(Helper.GetUserErrTuple(errCode));
        }
    }
}
