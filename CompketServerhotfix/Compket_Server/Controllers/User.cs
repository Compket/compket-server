﻿using Compket_Server.Base;
using Compket_Server.Systems;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Compket_Server.Base.ControllerInputStructs;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Compket_Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class User : ControllerBase
    {
        [HttpPost]
        [Route("ViewMarkets")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, List<Models.Market>>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult ViewMarkets([FromHeader] string accessToken, 
            [FromBody] dynamic query)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode))
            {
                var body = Helper.DynamicBodyToDynamic(query);
                string name = null;
                string phone = null;
                string locationCountry = null;
                string locationCity = null;
                try
                {
                    name = body[Defenitions.UserViewMarketsNameQuery];
                }
                catch
                {
                }
                try
                {
                    phone = body[Defenitions.UserViewMarketsPhoneQuery];
                }
                catch
                {
                }
                try
                {
                    locationCountry = body[Defenitions.UserViewMarketsLocationCountryQuery];
                }
                catch
                {
                }
                try
                {
                    locationCity = body[Defenitions.UserViewMarketsLocationCityQuery];
                }
                catch
                {
                }
                if (name == null && phone == null && locationCountry == null)
                {
                    return Ok(Helper.GetSuccessTuple(Markets.ListTopMarkets()));
                }
                else
                {
                    
                    return Ok(Helper.GetSuccessTuple(Markets.ListMarkets(locationCountry, locationCity, 
                        name, phone)));
                }


            }
            else
                return BadRequest(Helper.GetUserErrTuple(errCode));
        }

        [HttpGet]
        [Route("GetAllLocations")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, List<Models.Market>>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult GetAllLocations([FromHeader] string accessToken)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode))
            {
                return Ok(Helper.GetSuccessTuple(Markets.ListMarketLocations(), false));

            }
            else
                return BadRequest(Helper.GetUserErrTuple(errCode));
        }

        [HttpGet]
        [Route("GetMarketItems")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, List<Models.Item>>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult GetMarketItems([FromHeader] string accessToken, int mid)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode))
            {
                var items = Items.FindAllByMarketId(mid);
                if (items != null && items.Count > 0)
                    return Ok(Helper.GetSuccessTuple(items));
                else
                    return NotFound();
            }
            else
                return BadRequest(Helper.GetUserErrTuple(errCode));
        }
        
        [HttpPost]
        [Route("GetGenericItems")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, List<Models.Item>>))]

        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult GetGenericItems([FromHeader] string accessToken, [FromBody] GenericItemFiltering itemFiltering)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode))
            {
                var items = Items.FindAllByCategoryAndNameOrBarcode(itemFiltering.CategoryId, 
                    itemFiltering.Name, itemFiltering.Barcode);
                return Ok(Helper.GetSuccessTuple(items));
            }
            else
                return BadRequest(Helper.GetUserErrTuple(errCode));
        }
        
        [HttpPost]
        [Route("GenerateComparedLists")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Tuple<int, List<GenericItemComparisonResult>>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest,
            Type = typeof(Tuple<int, string>))]
        public IActionResult GenerateComparedLists([FromHeader] string accessToken, [FromBody] GenericItemComparisonFiltering itemFiltering)
        {
            if (Helper.ControllerTokenValidator(accessToken, out var _,
                out var foundToken, out int errCode))
            {
                var requiredMarkets = Markets.ListMarkets(itemFiltering.Country, itemFiltering.City);
                var foundLists = Items.GenerateListsForGivenMarketsWithGivenBarcodes(requiredMarkets,
                    itemFiltering.BarcodesLst);
                var retLst = new List<GenericItemComparisonResult>();
                foreach(var dict in foundLists)
                {
                    var lst = new List<GenericItemComparisonSpecificListResult>();
                    foreach(var val in dict.Value)
                    {
                        lst.Add(new GenericItemComparisonSpecificListResult()
                        {
                            RequestedItem = val.Key,
                            FoundItem = val.Value
                        });
                    }
                    retLst.Add(new GenericItemComparisonResult()
                    {
                        Market = dict.Key,
                        FoundItems = lst
                    });
                }
                return Ok(Helper.GetSuccessTuple(retLst));
            }
            else
                return BadRequest(Helper.GetUserErrTuple(errCode));
        }
    }
}
